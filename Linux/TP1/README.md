# TP1 : (re)Familiaration avec un système GNU/Linux

## 0. Préparation de la machine

**🌞 Setup de deux machines Rocky Linux configurées de façon basique.**

- un accès internet (via la carte NAT)

```
[clem@node1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
10.101.1.0/24 dev enp0s8 proto kernel scope link src 10.101.1.11 metric 101
```
```
[clem@node2 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
10.101.1.0/24 dev enp0s8 proto kernel scope link src 10.101.1.12 metric 101
```

- un accès à un réseau local

```
[clem@node1 ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.614 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=1.30 ms
^C
--- 10.101.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1051ms
rtt min/avg/max/mdev = 0.614/0.954/1.295/0.341 ms
```

- les machines doivent avoir un nom

```
[clem@node1 ~]$ hostname
node1.tp1.b2
```

```
[clem@node2 ~]$ hostname
node2.tp1.b2
```

- utiliser 1.1.1.1 comme serveur DNS

```
[clem@node1 ~]$ [clem@node1 ~]$ dig ynov.com
;; ANSWER SECTION:
# mettre en évidence la ligne qui contient la réponse : l'IP qui correspond au nom demandé
ynov.com.               4227    IN      A       92.243.16.143

;; Query time: 40 msec
# mettre en évidence la ligne qui contient l'adresse IP du serveur qui vous a répondu
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Wed Sep 22 16:53:52 CEST 2021
;; MSG SIZE  rcvd: 53
```

```
[clem@node2 ~]$ dig ynov.com

;; ANSWER SECTION:
ynov.com.               3469    IN      A       92.243.16.143

;; Query time: 32 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Wed Sep 22 17:06:31 CEST 2021
;; MSG SIZE  rcvd: 53
```

- les machines doivent pouvoir se joindre par leurs noms respectifs

```
[clem@node1 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.494 ms
64 bytes from node1.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.533 ms
^C
--- node1.tp1.b2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1028ms
rtt min/avg/max/mdev = 0.494/0.513/0.533/0.029 ms
```

```
[clem@node2 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=0.656 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=2 ttl=64 time=1.22 ms
^C
--- node1.tp1.b2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1061ms
rtt min/avg/max/mdev = 0.656/0.940/1.224/0.284 ms
```

- le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires

```
[clem@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

```
[clem@node2 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## I. Utilisateurs

### 1. Création et configuration

**🌞 Ajouter un utilisateur à la machine, qui sera dédié à son administration.**

`[clem@node1 ~]$ sudo useradd toto -m -s /bin/bash -u 2000`

**🌞 Créer un nouveau groupe admins qui contiendra les utilisateurs de la machine ayant accès aux droits de root via la commande sudo.**

`[clem@node1 ~]$ sudo groupadd admins`

`[clem@node1 ~]$ sudo visudo`

```
...
%admins ALL=(ALL)       ALL
...
```

**🌞 Ajouter votre utilisateur à ce groupe admins.**

```
[clem@node1 ~]$ sudo usermod -aG admins toto
[sudo] password for clem:
[clem@node1 ~]$ groups toto
toto : toto admins
```

### 2. SSH

**🌞 Pour cela :**

- il faut générer une clé sur le poste client de l'administrateur qui se connectera à distance

```
PS C:\Users\cleme> ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\cleme/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\cleme/.ssh/id_rsa.
Your public key has been saved in C:\Users\cleme/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:DsG7YaaU2BDvCo32RTTa8RRJ73yNpw7DiQdo1R4sEm4 cleme@LAPTOP-TMEGJIHJ
The key's randomart image is:
+---[RSA 4096]----+
|  .  =.+o        |
|   o=.*.+        |
|  ...Eo+ =       |
| o =o.+o= . o    |
|o.o =+*.S+ o o   |
|...oo+ =+ o o    |
|  ... ...* .     |
|        . +      |
|           .     |
+----[SHA256]-----+
```

- déposer la clé dans le fichier /home/USER/.ssh/authorized_keys de la machine que l'on souhaite administrer

```
PS C:\Users\cleme> cat C:\Users\cleme\.ssh\id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCv+nOqA9meLJKzhF0Nyx2pixwyftmohVkecHhPXBG9WqeahHzsbycJe5E+mXvXuRMnq4eY2hpZ4ZgRRBUb5YcszOOLBuUVLNbejAykzvkTkcIYn6u9IN3ETdNgYG3HeSKYMRV9vl8gJEdzm81CHnUiYI9wQytD/v70YP3ZsKZmv/ehoNaTXQLrLeOdssekbYzGLcsnv4mduLRzwUJYtcJqGCPqSPcbF4ocO9FEYdUYscCW404huY/3IRx7vdum0rRLR3TUAfKv1bHRMpKdOe+vKH0sKVMW1bdE+yK16xQQOy8Y7sxkrpBtgg26WvqFuMd/ZpidmtSncJRAriP4l/dQ7M+AeX2fiA46SKFMTfwUUPxIZNvuN3kTvj1GJeJNj4K5QrzlVbhsE64YCq4rp2OpwCa65M0eTrpp69JG4JgVmXakolh9dDN9wtP8vuVsNyzDOE5GVO7uiJl6A+0Q62YnFsNqFpcOnLV9r2nj9yOjbOY50VIOafMF1JiVab6WW/Rk02C5gTkt1+rkN3vWJ6G1YUz4rp6JxRYOiMmv0J9LJSFQRIXnj7TqnSWx5vQAe+TQ4emodSy0ajo5i3zmGTyEbT+QiHTHtyfadxjjYFETkJvjg0yc9pM78fJmzWWXj6/ImPGO5hTzRSnFpNZ83Ou7qvuuMqMKua6+oUXqlGPKnQ== cleme@LAPTOP-TMEGJIHJ
```

```
[clem@node1 ~]$ sudo mkdir /home/toto/.ssh
[clem@node1 ~]$ sudo nano /home/toto/.ssh/authorized_keys
[clem@node1 ~]$ sudo cat /home/toto/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCv+nOqA9meLJKzhF0Nyx2pixwyftmohVkecHhPXBG9WqeahHzsbycJe5E+mXvXuRMnq4eY2hpZ4ZgRRBUb5YcszOOLBuUVLNbejAykzvkTkcIYn6u9IN3ETdNgYG3HeSKYMRV9vl8gJEdzm81CHnUiYI9wQytD/v70YP3ZsKZmv/ehoNaTXQLrLeOdssekbYzGLcsnv4mduLRzwUJYtcJqGCPqSPcbF4ocO9FEYdUYscCW404huY/3IRx7vdum0rRLR3TUAfKv1bHRMpKdOe+vKH0sKVMW1bdE+yK16xQQOy8Y7sxkrpBtgg26WvqFuMd/ZpidmtSncJRAriP4l/dQ7M+AeX2fiA46SKFMTfwUUPxIZNvuN3kTvj1GJeJNj4K5QrzlVbhsE64YCq4rp2OpwCa65M0eTrpp69JG4JgVmXakolh9dDN9wtP8vuVsNyzDOE5GVO7uiJl6A+0Q62YnFsNqFpcOnLV9r2nj9yOjbOY50VIOafMF1JiVab6WW/Rk02C5gTkt1+rkN3vWJ6G1YUz4rp6JxRYOiMmv0J9LJSFQRIXnj7TqnSWx5vQAe+TQ4emodSy0ajo5i3zmGTyEbT+QiHTHtyfadxjjYFETkJvjg0yc9pM78fJmzWWXj6/ImPGO5hTzRSnFpNZ83Ou7qvuuMqMKua6+oUXqlGPKnQ== cleme@LAPTOP-TMEGJIHJ
[clem@node1 ~]$ sudo chmod 600 /home/toto/.ssh/authorized_keys
[clem@node1 ~]$ sudo chmod 700 /home/toto/.ssh
```

**🌞 Assurez vous que la connexion SSH est fonctionnelle, sans avoir besoin de mot de passe.**

```
PS C:\Users\cleme> ssh toto@10.101.1.11 -i 'C:/Users/cleme/.ssh/id_rsa'
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Sun Sep 26 19:04:15 2021 from 10.101.1.1
[toto@node1 ~]$
```

## II. Partitionnement

### 1. Préparation de la VM

Ajout de deux disques durs à la machine virtuelle, de 3Go chacun.

```
[clem@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
sr0          11:0    1 1024M  0 rom
```

### 2. Partitionnement

**🌞 Utilisez LVM pour :**

- agréger les deux disques en un seul volume group

```
[clem@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for clem:
  Physical volume "/dev/sdb" successfully created.
[clem@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
[clem@node1 ~]$ sudo vgcreate data /dev/sdb
  Volume group "data" successfully created
[clem@node1 ~]$ sudo vgextend data /dev/sdc
  Volume group "data" successfully extended
```

```
[clem@node1 ~]$ sudo vgs
  VG   #PV #LV #SN Attr   VSize  VFree
  data   2   3   0 wz--n-  5.99g 2.99g
  rl     1   2   0 wz--n- <7.00g    0
```

- créer 3 logical volumes de 1 Go chacun

```
[clem@node1 ~]$ sudo lvcreate -L 1G data -n part1
  Logical volume "part1" created.
[clem@node1 ~]$ sudo lvcreate -L 1G data -n part2
  Logical volume "part2" created.
[clem@node1 ~]$ sudo lvcreate -L 1G data -n part3
  Logical volume "part3" created.
```

```
[clem@node1 ~]$ sudo lvs
  LV    VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  part1 data -wi-ao----   1.00g
  part2 data -wi-ao----   1.00g
  part3 data -wi-ao----   1.00g
  root  rl   -wi-ao----  <6.20g
  swap  rl   -wi-ao---- 820.00m
```

- formater ces partitions en ext4

```
[clem@node1 ~]$ sudo mkfs -t ext4 /dev/data/part1
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: f6b2d860-b3af-4304-af43-a9f9ee52e57f
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[clem@node1 ~]$ sudo mkfs -t ext4 /dev/data/part2
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 1bffc604-1f26-495d-b756-5cbc9b813964
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[clem@node1 ~]$ sudo mkfs -t ext4 /dev/data/part3
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: ab40698b-6e5c-4893-95c0-9cced479117c
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

- monter ces partitions pour qu'elles soient accessibles aux points de montage /mnt/part1, /mnt/part2 et /mnt/part3.

```
[clem@node1 ~]$ sudo mkdir /mnt/part1
[clem@node1 ~]$ sudo mount /dev/data/part1 /mnt/part1
[clem@node1 ~]$ sudo mkdir /mnt/part2
[clem@node1 ~]$ sudo mount /dev/data/part2 /mnt/part2
[clem@node1 ~]$ sudo mkdir /mnt/part3
[clem@node1 ~]$ sudo mount /dev/data/part3 /mnt/part3
```

```
[clem@node1 ~]$ sudo mount
...
/dev/mapper/data-part1 on /mnt/part1 type ext4 (rw,relatime,seclabel)
/dev/mapper/data-part2 on /mnt/part2 type ext4 (rw,relatime,seclabel)
/dev/mapper/data-part3 on /mnt/part3 type ext4 (rw,relatime,seclabel)
```

**🌞 Grâce au fichier /etc/fstab, faites en sorte que cette partition soit montée automatiquement au démarrage du système.**

```
[clem@node1 ~]$ sudo cat /etc/fstab

...
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=5717caf0-5de3-4ed8-b974-f8bef73c4ebd /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
/dev/data/part1 /mnt/part1 ext4 defaults 0 0
/dev/data/part2 /mnt/part2 ext4 defaults 0 0
/dev/data/part3 /mnt/part3 ext4 defaults 0 0
```

```
[clem@node1 ~]$ sudo umount /mnt/part1
[clem@node1 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/part1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part1               : successfully mounted
/mnt/part2               : already mounted
/mnt/part3               : already mounted
```

## III. Gestion de services

### 1. Interaction avec un service existant

Parmi les services système déjà installés sur CentOS, il existe firewalld. Cet utilitaire est l'outil de firewalling de CentOS.

**🌞 Assurez-vous que :**

- l'unité est démarrée

```
[clem@node1 ~]$ sudo systemctl is-active firewalld
active
```

- l'unitée est activée (elle se lance automatiquement au démarrage)

```
[clem@node1 ~]$ sudo systemctl is-enabled firewalld
enabled
```

### 2. Création de service

#### A. Unité simpliste

**🌞 Créer un fichier qui définit une unité de service web.service dans le répertoire /etc/systemd/system.**

```
[clem@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[clem@node1 ~]$ sudo firewall-cmd --reload
success
[clem@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 8888/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

```
[clem@node1 ~]$ sudo nano /etc/systemd/system/web.service
[clem@node1 ~]$ sudo cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target

[clem@node1 ~]$ sudo systemctl start web
[clem@node1 ~]$ sudo systemctl enable web
[clem@node1 ~]$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: active (running) since Sun 2021-09-26 19:54:31 CEST; 6s ago
 Main PID: 2213 (python3)
    Tasks: 1 (limit: 11385)
   Memory: 9.5M
   CGroup: /system.slice/web.service
           └─2213 /bin/python3 -m http.server 8888

Sep 26 19:54:31 node1.tp1.b2 systemd[1]: Started Very simple web service.
```

**🌞 Une fois le service démarré, assurez-vous que pouvez accéder au serveur web : avec un navigateur ou la commande curl sur l'IP de la VM, port 8888.**

```
[clem@node1 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```

#### B. Modification de l'unité


**🌞 Créer un utilisateur web.**

`[clem@node1 ~]$ sudo useradd web -m -s /bin/sh`

**🌞 Modifiez l'unité de service web.service créée précédemment en ajoutant les clauses :**

```
[web@node1 ~]$ sudo mkdir /srv/doss
[web@node1 ~]$ sudo cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv/doss

[Install]
WantedBy=multi-user.target
```

**🌞 Placer un fichier de votre choix dans le dossier créé dans /srv et tester que vous pouvez y accéder une fois le service actif. Il faudra que le dossier et le fichier qu'il contient appartiennent à l'utilisateur web.**

```
[web@node1 ~]$ sudo cat /srv/doss/file
Fichier.
```

**🌞 Vérifier le bon fonctionnement avec une commande curl**

```
[clem@node1 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="file">file</a></li>
</ul>
<hr>
</body>
</html>
```