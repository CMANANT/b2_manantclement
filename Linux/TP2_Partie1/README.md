# TP2 pt. 1 : Gestion de service

# Sommaire

- [TP2 pt. 1 : Gestion de service](#tp2-pt-1--gestion-de-service)
- [Sommaire](#sommaire)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro](#1-intro)
  - [2. Setup](#2-setup)
    - [A. Serveur Web et NextCloud](#a-serveur-web-et-nextcloud)
    - [B. Base de données](#b-base-de-données)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

**🌞 Installer le serveur Apache**

- paquet httpd
- la conf se trouve dans /etc/httpd/
    - le fichier de conf principal est /etc/httpd/conf/httpd.conf
    - je vous conseille vivement de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair
        - avec vim vous pouvez tout virer avec :g/^ *#.*/d

            ```
            [clem@web ~]$ sudo cat /etc/httpd/conf/httpd.conf

            ServerRoot "/etc/httpd"

            Listen 80

            Include conf.modules.d/*.conf

            User apache
            Group apache


            ServerAdmin root@localhost


            <Directory />
                AllowOverride none
                Require all denied
            </Directory>


            DocumentRoot "/var/www/html"

            <Directory "/var/www">
                AllowOverride None
                Require all granted
            </Directory>

            <Directory "/var/www/html">
                Options Indexes FollowSymLinks

                AllowOverride None

                Require all granted
            </Directory>

            <IfModule dir_module>
                DirectoryIndex index.html
            </IfModule>

            <Files ".ht*">
                Require all denied
            </Files>

            ErrorLog "logs/error_log"

            LogLevel warn

            <IfModule log_config_module>
                LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
                LogFormat "%h %l %u %t \"%r\" %>s %b" common

                <IfModule logio_module>
                  LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
                </IfModule>


                CustomLog "logs/access_log" combined
            </IfModule>

            <IfModule alias_module>


                ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

            </IfModule>

            <Directory "/var/www/cgi-bin">
                AllowOverride None
                Options None
                Require all granted
            </Directory>

            <IfModule mime_module>
                TypesConfig /etc/mime.types

                AddType application/x-compress .Z
                AddType application/x-gzip .gz .tgz



                AddType text/html .shtml
                AddOutputFilter INCLUDES .shtml
            </IfModule>

            AddDefaultCharset UTF-8

            <IfModule mime_magic_module>
                MIMEMagicFile conf/magic
            </IfModule>


            EnableSendfile on

            IncludeOptional conf.d/*.conf
            ```
            
**🌞 Démarrer le service Apache**

- le service s'appelle httpd (raccourci pour httpd.service en réalité)
    - démarrez le
        `[clem@web ~]$ sudo systemctl start httpd`
    - faites en sorte qu'Apache démarre automatique au démarrage de la machine
        `[clem@web ~]$ sudo systemctl enable httpd`
    - ouvrez le port firewall nécessaire
        - utiliser une commande ss pour savoir sur quel port tourne actuellement Apache
        - une petite portion du mémo est consacrée à ss
            ```
            [clem@web ~]$ sudo ss -alnpt
            State       Recv-Q      Send-Q           Local Address:Port           Peer Address:Port     Process
            LISTEN      0           128                    0.0.0.0:22                  0.0.0.0:*         users:(("sshd",pid=823,fd=5))
            LISTEN      0           128                          *:80                        *:*         users:(("httpd",pid=5911,fd=4),("httpd",pid=5910,fd=4),("httpd",pid=5909,fd=4),("httpd",pid=5907,fd=4))
            LISTEN      0           128                       [::]:22                     [::]:*         users:(("sshd",pid=823,fd=7))
            [clem@web ~]$  sudo firewall-cmd --add-port=80/tcp --permanent
            success
            ```
        
**🌞 TEST**

- vérifier que le service est démarré
    ```
    [clem@web ~]$ systemctl is-active httpd
    active
    ```
- vérifier qu'il est configuré pour démarrer automatiquement
    ```
    [clem@web ~]$ systemctl is-enabled httpd
    enabled
    ```
- vérifier avec une commande curl localhost que vous joignez votre serveur web localement
    ```
    [clem@web ~]$ curl localhost
    <!doctype html>
    <html>
      <head>
    [...]
      </body>
    </html>
    ```
- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web
    `10.102.1.11:80`
    
## 2. Avancer vers la maîtrise du service

**🌞 Le service Apache...`**

- donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume
    `[clem@web ~]$ sudo systemctl start httpd`
- prouvez avec une commande qu'actuellement, le service est paramétré pour démarré quand la machine s'allume
    `[clem@web ~]$ sudo systemctl enable httpd`
- affichez le contenu du fichier httpd.service qui contient la définition du service Apache
    ```
    [clem@web ~]$ cat /usr/lib/systemd/system/httpd.service
    # See httpd.service(8) for more information on using the httpd service.

    # Modifying this file in-place is not recommended, because changes
    # will be overwritten during package upgrades.  To customize the
    # behaviour, run "systemctl edit httpd" to create an override unit.

    # For example, to pass additional options (such as -D definitions) to
    # the httpd binary at startup, create an override unit (as is done by
    # systemctl edit) and enter the following:

    #       [Service]
    #       Environment=OPTIONS=-DMY_DEFINE

    [Unit]
    Description=The Apache HTTP Server
    Wants=httpd-init.service
    After=network.target remote-fs.target nss-lookup.target httpd-init.service
    Documentation=man:httpd.service(8)

    [Service]
    Type=notify
    Environment=LANG=C

    ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
    ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
    # Send SIGWINCH for graceful stop
    KillSignal=SIGWINCH
    KillMode=mixed
    PrivateTmp=true

    [Install]
    WantedBy=multi-user.target
    ```
    
**🌞 Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf qui définit quel user est utilisé
    ```
    [clem@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
    [...]
    User apache
    [...]
    ```
- utilisez la commande ps -ef pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
    ```
    [clem@web ~]$ ps -ef | grep apache
    apache      5908    5907  0 17:02 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    apache      5909    5907  0 17:02 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    apache      5910    5907  0 17:02 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    apache      5911    5907  0 17:02 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    ```
- vérifiez avec un ls -al le dossier du site (dans /var/www/...)
    - vérifiez que tout son contenu est accessible à l'utilisateur mentionné dans le fichier de conf
        ```
        [clem@web ~]$ ls -al /var/www/
        total 4
        drwxr-xr-x.  4 root root   33 Sep 29 16:52 .
        drwxr-xr-x. 22 root root 4096 Sep 29 16:52 ..
        drwxr-xr-x.  2 root root    6 Jun 11 17:35 cgi-bin
        drwxr-xr-x.  2 root root    6 Jun 11 17:35 html
        ```
        Tous son contenu est exécutable par tout le monde dont l'utilisateur mentionné dans le fichier de conf.
        
**🌞 Changer l'utilisateur utilisé par Apache**

- créez le nouvel utilisateur
    - pour les options de création, inspirez-vous de l'utilisateur Apache existant
        - le fichier /etc/passwd contient les informations relatives aux utilisateurs existants sur la machine
        - servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut
            ```
            [clem@web ~]$ sudo cat /etc/passwd | grep apache
            apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
            [clem@web ~]$ sudo useradd toto -s /sbin
            ```
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
    ```
    [clem@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
    [...]
    User toto
    [...]
    ```
- redémarrez Apache
    ```
    [clem@web ~]$ sudo systemctl stop httpd
    [clem@web ~]$ sudo systemctl start httpd
    ```
- utilisez une commande ps pour vérifier que le changement a pris effet
    ```
    [clem@web ~]$ ps -ef | grep toto
    toto        2076    2075  0 15:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    toto        2077    2075  0 15:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    toto        2078    2075  0 15:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    toto        2079    2075  0 15:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    ```
    
**🌞 Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port de votre choix
    ```
    [clem@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
    [...]
    Listen 9589
    [...]
    ```
- ouvrez un nouveau port firewall, et fermez l'ancien
    ```
    [clem@web ~]$ sudo firewall-cmd --add-port=9589/tcp --permanent
    success
    [clem@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
    success
    [clem@web ~]$ sudo firewall-cmd --list-all
    public (active)
      target: default
      icmp-block-inversion: no
      interfaces: enp0s3 enp0s8
      sources:
      services: ssh
      ports: 9589/tcp
      protocols:
      masquerade: no
      forward-ports:
      source-ports:
      icmp-blocks:
      rich rules:
    ```
- redémarrez Apache
    ```
    [clem@web ~]$ sudo systemctl stop httpd
    [clem@web ~]$ sudo systemctl start httpd
    ```
- prouvez avec une commande ss que Apache tourne bien sur le nouveau port choisi
    ```
    [clem@web ~]$ sudo ss -alnpt
    State       Recv-Q      Send-Q            Local Address:Port             Peer Address:Port      Process
    LISTEN      0           128                     0.0.0.0:22                    0.0.0.0:*          users:(("sshd",pid=833,fd=5))
    LISTEN      0           128                           *:9589                        *:*          users:(("httpd",pid=2364,fd=4),("httpd",pid=2363,fd=4),("httpd",pid=2362,fd=4),("httpd",pid=2360,fd=4))
    LISTEN      0           128                        [::]:22                       [::]:*          users:(("sshd",pid=833,fd=7))
    ```
- vérifiez avec curl en local que vous pouvez joindre Apache sur le nouveau port
    ```
    [clem@web ~]$ curl 10.102.1.11:9589
    <!doctype html>
    <html>
      <head>
        [...]
      </body>
    </html>
    ```
- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port
    `http://10.102.1.11:9589/`
    
**📁 Fichier /etc/httpd/conf/httpd.conf :**
```
[clem@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
[sudo] password for clem:

ServerRoot "/etc/httpd"

Listen 9589

Include conf.modules.d/*.conf

User toto
Group apache


ServerAdmin root@localhost


<Directory />
    AllowOverride none
    Require all denied
</Directory>


DocumentRoot "/var/www/html"

<Directory "/var/www">
    AllowOverride None
    Require all granted
</Directory>

<Directory "/var/www/html">
    Options Indexes FollowSymLinks

    AllowOverride None

    Require all granted
</Directory>

<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

<Files ".ht*">
    Require all denied
</Files>

ErrorLog "logs/error_log"

LogLevel warn

<IfModule log_config_module>
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common

    <IfModule logio_module>
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>


    CustomLog "logs/access_log" combined
</IfModule>

<IfModule alias_module>


    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

</IfModule>

<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>
    TypesConfig /etc/mime.types

    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz



    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

AddDefaultCharset UTF-8

<IfModule mime_magic_module>
    MIMEMagicFile conf/magic
</IfModule>


EnableSendfile on

IncludeOptional conf.d/*.conf
IncludeOptional sites-enabled/*
```

## II. Une stack web plus avancée

**⚠⚠⚠ Réinitialiser votre conf Apache avant de continuer ⚠⚠⚠**

- reprendre le port par défaut
- reprendre l'utilisateur par défaut

    ```
    [clem@web ~]$ sudo cat /etc/httpd/conf/httpd.conf

    ServerRoot "/etc/httpd"

    Listen 80

    [...]

    User apache
    [...]
    [clem@web ~]$ sudo firewall-cmd --list-all
    public (active)
    [...]
      ports: 80/tcp
    ```

### 1. Intro

### 2. Setup

#### A. Serveur Web et NextCloud

**🌞 Install du serveur Web et de NextCloud sur web.tp2.linux**

- n'oubliez pas de réinitialiser votre conf Apache avant de continuer
    - remettez le port et le user par défaut en particulier
    ```
    [clem@web ~]$ sudo cat /etc/httpd/conf/httpd.conf

    ServerRoot "/etc/httpd"

    Listen 80

    [...]

    User apache
    [...]
    [clem@web ~]$ sudo firewall-cmd --list-all
    public (active)
    [...]
      ports: 80/tcp
    ```
- déroulez la doc d'install de Rocky
    - uniquement pour le serveur Web + NextCloud, vous ferez la base de données MariaDB après
    - quand ils parlent de la base de données, juste vous sautez l'étape, on le fait après :)
- je veux dans le rendu toutes les commandes réalisées
    - n'oubliez pas la commande history qui permet de voir toutes les commandes tapées précédemment

        ```
        [clem@web nextcloud]$ history
        [...]
           50  sudo dnf -y install epel-release
           51  sudo dnf update
           52  sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
           53  sudo module list php
           54  sudo dnf module list php
           55  sudo dnf module enable php:remi-7.4
           56  sudo dnf module list php
           57  sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp -y
           58  sudo systemctl enable httpd
           59  sudo dnf install httpd php
           60  sudo mkdir /etc/httpd/sites-available
           61  sudo mkdir /etc/httpd/sites-enabled
           62  sudo vim /etc/httpd/conf/httpd.conf
           63  sudo cat /etc/httpd/conf/httpd.conf
           64  sudo mkdir /var/www/sub-domains/
           65  sudo vim /etc/httpd/sites-available/linux.tp2.web
           66  sudo cat /etc/httpd/sites-available/linux.tp2.web
           67  sudo mkdir -p /var/www/sub-domains/linux.tp2.web/html
           68  sudo cp -Rf /etc/httpd/sites-available/ /var/www/sub-domains/linux.tp2.web/html/
           69  sudo vim /etc/httpd/sites-available/linux.tp2.web
           70  sudo cat /etc/httpd/sites-available/linux.tp2.web
           71  ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/
           72  sudo ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/
           73  sudo mkdir -p /var/www/sub-domains/linux.tp2.linux/html
           74  cd /usr/share/zoneinfo
           75  sudo vim /etc/opt/remi/php74/php.ini
           76  ls -al /etc/localtime
           77  cd /usr/clem
           78  cd /home/clem
           79  sudo wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
           80  sudo unzip nextcloud-21.0.1.zip
           81  cd nextcloud/
           82  sudo mv * /var/www/sub-domains/linux.tp2.web/html/
           83  sudo chown -Rf apache.apache /var/www/sub-domains/linux.tp2.web/html
           84  sudo mv /var/www/sub-domains/linux.tp2.web/html/data /var/www/sub-domains/linux.tp2.web/
           85  ls -al /var/www/sub-domains/linux.tp2.web/html/
           86  sudo mkdir /var/www/sub-domains/linux.tp2.web/html/data
           87  sudo chmod 755 /var/www/sub-domains/linux.tp2.web/html/data/
           88  ls -al /var/www/sub-domains/linux.tp2.web/html/
           89  sudo mv /var/www/sub-domains/linux.tp2.web/html/data /var/www/sub-domains/linux.tp2.web/
           90  sudo plain text systemctl restart httpd
           91  sudo systemctl restart httpd
           92  history
        ```
        
**📁 Fichier /etc/httpd/conf/httpd.conf :**
```
[clem@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
[sudo] password for clem:

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache


ServerAdmin root@localhost


<Directory />
    AllowOverride none
    Require all denied
</Directory>


DocumentRoot "/var/www/html"

<Directory "/var/www">
    AllowOverride None
    Require all granted
</Directory>

<Directory "/var/www/html">
    Options Indexes FollowSymLinks

    AllowOverride None

    Require all granted
</Directory>

<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

<Files ".ht*">
    Require all denied
</Files>

ErrorLog "logs/error_log"

LogLevel warn

<IfModule log_config_module>
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common

    <IfModule logio_module>
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>


    CustomLog "logs/access_log" combined
</IfModule>

<IfModule alias_module>


    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

</IfModule>

<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>
    TypesConfig /etc/mime.types

    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz



    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

AddDefaultCharset UTF-8

<IfModule mime_magic_module>
    MIMEMagicFile conf/magic
</IfModule>


EnableSendfile on

IncludeOptional conf.d/*.conf
IncludeOptional sites-enabled/*
```

**📁 Fichier /etc/httpd/sites-available/web.tp2.linux :**

```
[clem@web ~]$ sudo cat /etc/httpd/sites-available/linux.tp2.web
<VirtualHost *:80>
  DocumentRoot /var/www/sub-domains/linux.tp2.web/html/
  ServerName  web.tp2.linux

  <Directory /var/www/sub-domains/linux.tp2.web/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

#### B. Base de données

**🌞 Install de MariaDB sur db.tp2.linux**

- déroulez la doc d'install de Rocky
- manipulation
- je veux dans le rendu toutes les commandes réalisées
    ```
    [clem@db ~]$ history
        1  sudo firewall-cmd --list-all
        2  sudo firewall-cmd --remove-service=cockpit --permanent
        3  sudo firewall-cmd --remove-service=dhvpv6-client --permanent
        4  sudo firewall-cmd --remove-service=dhcpv6-client --permanent
        5  sudo firewall-cmd --reload
        6  sudo firewall-cmd --list-all
        7  ping -c 2 8.8.8.8
        8  dig ynov.com
        9  sudo vim /etc/resolv.conf
       10  sudo dnf install mariadb-server
       11  sudo systemctl enable mariadb
       12  sudo systemctl start mariadb
       13  sudo mysql_secure_installation
       14  history
    ```
- vous repérerez le port utilisé par MariaDB avec une commande ss exécutée sur db.tp2.linux
    ```
    [clem@db ~]$ sudo ss -alnpt
    State             Recv-Q             Send-Q                         Local Address:Port                         Peer Address:Port            Process
    [...]
    LISTEN            0                  80                                         *:3306                                    *:*                users:(("mysqld",pid=30146,fd=21))
    ```
    
**🌞 Préparation de la base pour NextCloud**

- une fois en place, il va falloir préparer une base de données pour NextCloud :
    - connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root -p`
    - exécutez les commandes SQL suivantes :

        ```
        [clem@db ~]$ sudo mysql -u root -p
        [...]
        MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
        Query OK, 0 rows affected (0.000 sec)

        MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
        Query OK, 1 row affected (0.001 sec)

        MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
        Query OK, 0 rows affected (0.000 sec)

        MariaDB [(none)]> FLUSH PRIVILEGES;
        Query OK, 0 rows affected (0.001 sec)
        ```

**🌞 Exploration de la base de données**

- afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :
    - depuis la machine web.tp2.linux vers l'IP de db.tp2.linux
    - vous pouvez utiliser la commande mysql pour vous connecter à une base de données depuis la ligne de commande
        - par exemple mysql -u USER -h IP_DATABASE -p
            ```
            [clem@web nextcloud]$ mysql -u nextcloud -h 10.102.1.12 -p
            Enter password:
            Welcome to the MariaDB monitor.  Commands end with ; or \g.
            Your MariaDB connection id is 25
            Server version: 10.3.28-MariaDB MariaDB Server

            Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

            Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

            MariaDB [(none)]>
            ```
- utilisez les commandes SQL fournies ci-dessous pour explorer la base

    ```
    MariaDB [(none)]> SHOW DATABASES;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | nextcloud          |
    +--------------------+
    2 rows in set (0.002 sec)

    MariaDB [(none)]> USE nextcloud;
    Database changed
    MariaDB [nextcloud]> SHOW TABLES;
    Empty set (0.001 sec)
    ```
    
- trouver une commande qui permet de lister tous les utilisateurs de la base de données

```
MariaDB [(none)]> SELECT User FROM mysql.user;
+-----------+
| User      |
+-----------+
| nextcloud |
| root      |
| root      |
| root      |
+-----------+
4 rows in set (0.001 sec)
```

#### C. Finaliser l'installation de NextCloud

**🌞 sur votre PC**

- modifiez votre fichier hosts (oui, celui de votre PC, de votre hôte)
    - pour pouvoir joindre l'IP de la VM en utilisant le nom web.tp2.linux
        ```
        #
        127.0.0.1 localhost
        ::1 localhost
        10.102.1.11 web.tp2.linux
        ```
- avec un navigateur, visitez NextCloud à l'URL http://web.tp2.linux
    - c'est possible grâce à la modification de votre fichier hosts
- on va vous demander un utilisateur et un mot de passe pour créer un compte admin
    - ne saisissez rien pour le moment
- cliquez sur "Storage & Database" juste en dessous
    - choisissez "MySQL/MariaDB"
    - saisissez les informations pour que NextCloud puisse se connecter avec votre base
- saisissez l'identifiant et le mot de passe admin que vous voulez, et validez l'installation

**🌞 Exploration de la base de données**

- connectez vous en ligne de commande à la base de données après l'installation terminée
- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
    - bonus points si la réponse à cette question est automatiquement donnée par une requête SQL
        ```
        MariaDB [information_schema]> select count(*) from tables where table_schema='nextcloud';
        +----------+
        | count(*) |
        +----------+
        |       77 |
        +----------+
        ```

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          | Toutes        |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 3306        | 10.102.1.11   |
