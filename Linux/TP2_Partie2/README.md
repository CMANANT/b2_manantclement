# TP2 pt. 2 : Maintien en condition opérationnelle

## I. Monitoring


| Machine         | IP            | Service                 | Port ouvert | IPs autorisées |
|-----------------|---------------|-------------------------|-------------|----------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | ?           | ?              |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | ?           | ?              |

### 2. Setup

**🌞 Setup Netdata**

- y'a plein de méthodes d'install pour Netdata
- on va aller au plus simple, exécutez, sur toutes les machines que vous souhaitez monitorer : 

    ```
    [clem@web ~]$ sudo su -
    Last login: Mon Oct 11 15:36:41 CEST 2021 on pts/0
    [root@web ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
    [...]
    [root@web ~]# exit
    logout
    ```

    ```
    [clem@db ~]$ sudo su -
    Last login: Mon Oct 11 15:37:43 CEST 2021 on pts/0
    [root@web ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
    [...]
    [root@db ~]# exit
    logout
    ```

**🌞 Manipulation du service Netdata**

- un service netdata a été créé
- déterminer s'il est actif, et s'il est paramétré pour démarrer au boot de la machine
    - si ce n'est pas le cas, faites en sorte qu'il démarre au boot de la machine
        ```
        [clem@web ~]$ sudo systemctl status netdata
        ● netdata.service - Real time performance monitoring
           Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
           Active: active (running) since Mon 2021-10-11 15:37:58 CEST; 8min ago
        ```
        Il est actif et est paramétré pour démarrer au boot de la machine : 
        `Active: active (running)`
        `enabled;`
- déterminer à l'aide d'une commande ss sur quel port Netdata écoute
    ```
    [clem@web ~]$ sudo ss -alnpt
    State        Recv-Q       Send-Q              Local Address:Port                Peer Address:Port       Process
    [...]
    LISTEN       0            128                       0.0.0.0:19999                    0.0.0.0:*           users:(("netdata",pid=2280,fd=5))
    [...]
    ```
- autoriser ce port dans le firewall
    ```
    [clem@web ~]$ sudo firewall-cmd --add-port=19999/tcp
    success
    [clem@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
    success
    [clem@web ~]$ sudo firewall-cmd --list-all
    public (active)
      target: default
      icmp-block-inversion: no
      interfaces: enp0s3 enp0s8
      sources:
      services: ssh
      ports: 80/tcp 19999/tcp
      protocols:
      masquerade: no
      forward-ports:
      source-ports:
      icmp-blocks:
      rich rules:
    ```
    
**🌞 Setup Alerting**# discord (discordapp.com) global notification options

- ajustez la conf de Netdata pour mettre en place des alertes Discord
    - ui ui c'est bien ça : vous recevrez un message Discord quand un seul critique est atteint
- c'est là que ça se passe dans la doc de Netdata
    - n'oubliez pas que la conf se trouve pour nous dans /opt/netdata/etc/netdata/
        ```
        # discord (discordapp.com) global notification options

        # multiple recipients can be given like this:
        #                  "CHANNEL1 CHANNEL2 ..."

        # enable/disable sending discord notifications
        SEND_DISCORD="YES"

        # Create a webhook by following the official documentation -
        # https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
        DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897130330086047755/fD_Ly5iDII_KmGD-tuZSoKgvtSijqQIyKWE8tFF7BorTpXiTNUmCNtmlRh1MtHYmHdAr"

        # if a role's recipients are not configured, a notification will be send to
        # this discord channel (empty = do not send a notification for unconfigured
        # roles):
        DEFAULT_RECIPIENT_DISCORD="alert_clement"
        ```
- vérifiez le bon fonctionnement de l'alerting sur Discord
    - en suivant cette section de la doc
        ```
        [clem@web ~]$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

        # SENDING TEST WARNING ALARM TO ROLE: sysadmin
        2021-10-11 16:54:16: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is WARNING to 'alert_clement'
        # OK

        # SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
        2021-10-11 16:54:16: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CRITICAL to 'alert_clement'
        # OK

        # SENDING TEST CLEAR ALARM TO ROLE: sysadmin
        2021-10-11 16:54:17: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CLEAR to 'alert_clement'
        # OK
        ```

**🌞 Config alerting**

- créez une nouvelle alerte pour recevoir une alerte à 50% de remplissage de la RAM
    ```
    [clem@web health.d]$ sudo vim ram.conf
    [...]
    warn: $this > (($status >= $WARNING)  ? (50) : (80))
    crit: $this > (($status == $CRITICAL) ? (80) : (98))
    [...]
    ```
- testez que votre alerte fonctionne
    - il faudra remplir artificiellement la RAM pour voir si l'alerte remonte correctement
    - sur Linux, on utilise la commande stress pour ça
        ```
        [clem@web health.d]$ sudo stress --vm 4 --vm-bytes 1024M
        stress: info: [4198] dispatching hogs: 0 cpu, 0 io, 4 vm, 0 hdd
        stress: FAIL: [4198] (415) <-- worker 4201 got signal 9
        stress: WARN: [4198] (417) now reaping child worker processes
        stress: FAIL: [4198] (421) kill error: No such process
        stress: FAIL: [4198] (415) <-- worker 4202 got signal 9
        stress: WARN: [4198] (417) now reaping child worker processes
        stress: FAIL: [4198] (421) kill error: No such process
        stress: FAIL: [4198] (415) <-- worker 4199 got signal 9
        stress: WARN: [4198] (417) now reaping child worker processes
        stress: FAIL: [4198] (421) kill error: No such process
        ```

## II. Backup

### 2. Partage NFS

**🌞 Setup environnement**

- créer un dossier /srv/backup/
    `[clem@backup ~]$ sudo mkdir /srv/backup/`
- il contiendra un sous-dossier ppour chaque machine du parc
    - commencez donc par créer le dossier /srv/backup/web.tp2.linux/
        `[clem@backup ~]$ sudo mkdir /srv/backup/web.tp2.linux/`
- il existera un partage NFS pour chaque machine (principe du moindre privilège)
    `[clem@backup ~]$ sudo mkdir /srv/backup/db.tp2.linux/`
    
**🌞 Setup partage NFS**

- je crois que vous commencez à connaître la chanson... Google "nfs server rocky linux"
    - ce lien me semble être particulièrement simple et concis
        ```
        [clem@backup ~]$ sudo systemctl status rpcbind
        ● rpcbind.service - RPC Bind
           Loaded: loaded (/usr/lib/systemd/system/rpcbind.service; enabled; vendor preset: enabled)
           Active: active (running) since Tue 2021-10-12 16:19:34 CEST; 25s ago
             [...]
        [clem@backup ~]$ sudo systemctl status nfs-server
        ● nfs-server.service - NFS server and services
           Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; vendor preset: disabled)
          Drop-In: /run/systemd/generator/nfs-server.service.d
                   └─order-with-mounts.conf
           Active: active (exited) since Tue 2021-10-12 16:19:35 CEST; 33s ago
          [...]
        ```

**🌞 Setup points de montage sur web.tp2.linux**

- sur le même site, y'a ça
- monter le dossier /srv/backups/web.tp2.linux du serveur NFS dans le dossier /srv/backup/ du serveur Web
    `[clem@web ~]$ sudo mount -t nfs backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup/`
- vérifier...
    - avec une commande mount que la partition est bien montée
        ```
        [clem@web ~]$ mount | grep "srv"
        backup.tp2.linux:/srv/backup/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)
        ```
    - avec une commande df -h qu'il reste de la place
        ```
        [clem@web ~]$ df -h
        Filesystem                                  Size  Used Avail Use% Mounted on
        [...]
        backup.tp2.linux:/srv/backup/web.tp2.linux  6.2G  2.3G  4.0G  36% /srv/backup
        ```
    - avec une commande touch que vous avez le droit d'écrire dans cette partition
        `[clem@web ~]$ touch /srv/backup/hello`
- faites en sorte que cette partition se monte automatiquement grâce au fichier /etc/fstab
    ```
    [clem@web ~]$ sudo cat /etc/fstab
    [...]
    backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup    nfs     defaults     0 0
    ```

### 3. Backup de fichiers

**🌞 Rédiger le script de backup /srv/tp2_backup.sh**

    ```
    [clem@backup ~]$ cat tp2_backup.sh
    #!/bin/bash
    # Simple backup script
    # clementmanant ~ 15/12/21

    here=$1
    tobackup=$2

    if [ -z "$tobackup" ]; then
            echo "Il faut 2 dossiers en argument"
            exit 0
    fi

    if [ -d "$here" ]; then
            file="${tobackup}_$(date '+%y-%m-%d_%H-%M-%S').tar.gz"
            tar -czf "$file" "$tobackup"
            rsync -av $file $here
            echo "Archive successfully created."
            else
            echo "Le dossier de destination n'existe pas: $here"
    fi
    ```
    
![**📁 Fichier /srv/tp2_backup.sh**](./scripts/tp2_backup.sh)

**🌞 Tester le bon fonctionnement**

- exécuter le script sur le dossier de votre choix
    ```
    [clem@backup ~]$ sudo bash ./tp2_backup.sh here/ tobackup/
    sending incremental file list
    tp2_backup_21-12-17_16-19-09.tar.gz

    sent 284 bytes  received 35 bytes  638.00 bytes/sec
    total size is 163  speedup is 0.51
    Archive successfully created.
    ```
- prouvez que la backup s'est bien exécutée
    ```
    [clem@backup ~]$ ls here/
    tp2_backup_21-12-17_16-19-09.tar.gz
    ```
- tester de restaurer les données
    - récupérer l'archive générée, et vérifier son contenu
        ```
        [clem@backup ~]$ cd here/
        [clem@backup here]$ ls
        tp2_backup_21-12-17_16-19-09.tar.gz
        [clem@backup here]$ sudo tar xzvf tp2_backup_21-12-17_16-19-09.tar.gz
        tobackup/
        tobackup/file
        [clem@backup here]$ ls
        tobackup  tp2_backup_21-12-17_16-19-09.tar.gz
        [clem@backup here]$ cat tobackup/file
        Hello
        ```
        
### 4. Unité de service

#### A. Unité de service

**🌞 Créer une unité de service pour notre backup**

- c'est juste un fichier texte hein
- doit se trouver dans le dossier /etc/systemd/system/
- doit s'appeler tp2_backup.service
- le contenu :
    ```
    [clem@backup ~]$ cat /etc/systemd/system/tp2_backup.service
    [Unit]
    Description=Our own lil backup service (TP2)

    [Service]
    ExecStart=sudo bash /home/clem/tp2_backup.sh /home/clem/here/ /home/clem/tobackup/
    Type=oneshot
    RemainAfterExit=no

    [Install]
    WantedBy=multi-user.target
    ```
    
**🌞 Tester le bon fonctionnement**

- n'oubliez pas d'exécuter sudo systemctl daemon-reload à chaque ajout/modification d'un service
    `[clem@backup ~]$ sudo systemctl daemon-reload`
- essayez d'effectuer une sauvegarde avec sudo systemctl start backup
    `[clem@backup ~]$ sudo systemctl start tp2_backup`
- prouvez que la backup s'est bien exécutée
    - vérifiez la présence de la nouvelle archive
        ```
        [clem@backup ~]$ ls here/
        tobackup  tp2_backup_21-12-17_16-19-09.tar.gz  tp2_backup_21-12-17_16-35-53.tar.gz
        ```
        
#### B. Timer

**🌞 Créer le timer associé à notre tp2_backup.service**

- toujours juste un fichier texte
- dans le dossier /etc/systemd/system/ aussi
- fichier tp2_backup.timer
- contenu du fichier : 
    ```
    [clem@backup ~]$ cat /etc/systemd/system/tp2_backup.timer
    [Unit]
    Description=Periodically run our TP2 backup script
    Requires=tp2_backup.service

    [Timer]
    Unit=tp2_backup.service
    OnCalendar=*-*-* *:*:00

    [Install]
    WantedBy=timers.target
    ```
    
**🌞 Activez le timer**

- démarrer le timer : sudo systemctl start tp2_backup.timer
    `[clem@backup ~]$ sudo systemctl start tp2_backup.timer`
- activer le au démarrage avec une autre commande systemctl
    ```
    [clem@backup ~]$ sudo systemctl enable tp2_backup.timer
    Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.
    ```
- prouver que...
    - le timer est actif actuellement
        ```
        [clem@backup ~]$ sudo systemctl status tp2_backup.timer
        [...]
           Active: active (running) since Fri 2021-12-17 16:43:49 CET; 1min 23s [...]
        ```
    - qu'il est paramétré pour être actif dès que le système boot
        ```
        [clem@backup ~]$ sudo systemctl status tp2_backup.timer
        ● tp2_backup.timer - Periodically run our TP2 backup script
           Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
        [...]
        ```
        
**🌞 Tests !**

- avec la ligne OnCalendar=*-*-* *:*:00, le timer déclenche l'exécution du service toutes les minutes
- vérifiez que la backup s'exécute correctement
    ```
    [clem@backup ~]$ ls here/
    tobackup                             tp2_backup_21-12-17_16-44-08.tar.gz
    tp2_backup_21-12-17_16-19-09.tar.gz  tp2_backup_21-12-17_16-45-12.tar.gz
    tp2_backup_21-12-17_16-35-53.tar.gz  tp2_backup_21-12-17_16-46-34.tar.gz
    tp2_backup_21-12-17_16-43-49.tar.gz
    ```

#### C. Contexte

**🌞 Faites en sorte que...**

- votre backup s'exécute sur la machine web.tp2.linux
- le dossier sauvegardé est celui qui contient le site NextCloud (quelque part dans /var/)
- la destination est le dossier NFS monté depuis le serveur backup.tp2.linux
    ```
    [clem@web ~]$ sudo cat /etc/systemd/system/tp2_backup.service
    [Unit]
    Description=Our own lil backup service (TP2)

    [Service]
    ExecStart=sudo bash /srv/tp2_backup.sh /srv/backup/ /home/clem/nextcloud/
    Type=oneshot
    RemainAfterExit=no

    [Install]
    WantedBy=multi-user.target
    ```
- la sauvegarde s'exécute tous les jours à 03h15 du matin
    ```
    [clem@web ~]$ sudo cat /etc/systemd/system/tp2_backup.timer
    [Unit]
    Description=Periodically run our TP2 backup script
    Requires=tp2_backup.service

    [Timer]
    Unit=tp2_backup.service
    OnCalendar=*-*-* 3:15:00

    [Install]
    WantedBy=timers.target
    ```
- prouvez avec la commande sudo systemctl list-timers que votre service va bien s'exécuter la prochaine fois qu'il sera 03h15
    ```
    [clem@web ~]$ sudo systemctl list-timers
    NEXT                         LEFT       LAST                         PASSED       UNIT                    >
    Fri 2021-12-17 17:42:31 CET  30min left Fri 2021-12-17 16:39:10 CET  33min ago    dnf-makecache.timer     >
    Sat 2021-12-18 03:15:00 CET  10h left   n/a                          n/a          tp2_backup.timer        >
    Sat 2021-12-18 15:12:58 CET  22h left   Fri 2021-12-17 15:12:58 CET  1h 59min ago systemd-tmpfiles-clean.t>

    3 timers listed.
    Pass --all to see loaded but inactive timers, too.
    ```

![**📁 Fichier /etc/systemd/system/tp2_backup.timer**](./conf/tp2_backup.timer)

![**📁 Fichier /etc/systemd/system/tp2_backup.service**](./conf/tp2_backup.service)

### 5. Backup de base de données

**🌞 Création d'un script /srv/tp2_backup_db.sh**

```
[clem@db ~]$ cat /srv/tp2_backup_db.sh
#!/bin/bash
# Simple backup script
# clementmanant ~ 15/12/21

here=$1
tobackup=$2

if [ -d "$here" ]; then
        file="tp2_backup_$(date '+%y-%m-%d_%H-%M-%S').tar.gz"
        mysqldump --user=nextcloud --password=meow --${tobackup} > backup_donnees.sql
        tar -czf "$file" backup_donnees.sql
        rsync -av $file $here
        echo "Archive successfully created."
        else
        echo "ATTENTION: Le dossier de destination n'existe pas: $here"
fi
```

![**📁 Fichier /srv/tp2_backup_db.sh**](./scripts/tp2_backup_db.sh)

🌞 Restauration

- tester la restauration de données
- c'est à dire, une fois la sauvegarde effectuée, et le tar.gz en votre possession, tester que vous êtes capables de restaurer la base dans l'état au moment de la sauvegarde
    - il faut réinjecter le fichier .sql dans la base à l'aide d'une commmande mysql
        ```
        [clem@db ~]$ cd /srv/backup/
        [clem@db backup]$ ls
        test  tp2_backup_21-12-17_18-05-26.tar.gz  tp2_backup_21-12-17_18-07-26.tar.gz
        [clem@db backup]$ sudo tar zxvf tp2_backup_21-12-17_18-07-26.tar.gz
        [sudo] password for clem:
        backup_donnees.sql
        [clem@db backup]$ ls
        backup_donnees.sql  test  tp2_backup_21-12-17_18-05-26.tar.gz  tp2_backup_21-12-17_18-07-26.tar.gz
        [clem@db backup]$ mysql -u root -p nextcloud < backup_donnees.sql
        ```
        
**🌞 Unité de service**

- pareil que pour la sauvegarde des fichiers ! On va faire de ce script une unité de service.
- votre script /srv/tp2_backup_db.sh doit pouvoir se lancer grâce à un service tp2_backup_db.service
    ```
    [clem@db ~]$ cat /etc/systemd/system/tp2_backup_db.service
    [Unit]
    Description=Our own lil backup service (TP2)

    [Service]
    ExecStart=sudo bash srv/tp2_backup_db.sh /srv/backup/ nextcloud
    Type=oneshot
    RemainAfterExit=no

    [Install]
    WantedBy=multi-user.target
    ```
- le service est exécuté tous les jours à 03h30 grâce au timer tp2_backup_db.timer
    ```
    [clem@db ~]$ cat /etc/systemd/system/tp2_backup_db.timer
    [Unit]
    Description=Periodically run our TP2 backup script
    Requires=tp2_backup_db.service

    [Timer]
    Unit=tp2_backup_db.service
    OnCalendar=*-*-* 3:30:00

    [Install]
    WantedBy=timers.target
    ```
- prouvez le bon fonctionnement du service ET du timer
    ```
    [clem@db ~]$ sudo systemctl start tp2_backup_db.service
    [clem@db ~]$ ls /srv/backup/
    backup_donnees.sql                   tp2_backup_21-12-17_18-20-38.tar.gz
    test                                 tp2_backup_21-12-17_18-20-53.tar.gz
    tp2_backup_21-12-17_18-05-26.tar.gz  tp2_backup_21-12-17_18-24-07.tar.gz
    tp2_backup_21-12-17_18-07-26.tar.gz  tp2_backup_21-12-17_18-26-26.tar.gz
    [clem@db ~]$ sudo systemctl list-timers
    NEXT                         LEFT     LAST                         PASSED       UNIT                      >
    Sat 2021-12-18 03:30:00 CET  9h left  n/a                          n/a          tp2_backup_db.timer       >
    Sat 2021-12-18 15:13:43 CET  20h left Fri 2021-12-17 15:13:43 CET  3h 13min ago systemd-tmpfiles-clean.tim>
    n/a                          n/a      n/a                          n/a          dnf-makecache.timer       >

    3 timers listed.
    Pass --all to see loaded but inactive timers, too.
    ```
    
![**📁 Fichier /etc/systemd/system/tp2_backup_db.timer**](./conf/tp2_backup_db.timer)

![**📁 Fichier /etc/systemd/system/tp2_backup_db.service**](./conf/tp2_backup_db.service)
    
## III. Reverse Proxy
### 1. Introooooo

### 2. Setup simple

**🌞 Installer NGINX**

- vous devrez d'abord installer le paquet epel-release avant d'installer nginx
    - EPEL c'est des dépôts additionnels pour Rocky
    - NGINX n'est pas présent dans les dépôts par défaut que connaît Rocky
- le fichier de conf principal de NGINX est /etc/nginx/nginx.conf
    ```
    [clem@front ~]$ sudo dnf install -y epel-release
    [...]
    [clem@front ~]$ sudo dnf install -y nginx
    [...]
    ```
    
**🌞 Tester !**

- lancer le service nginx
    `[clem@front ~]$ sudo systemctl start nginx`
- le paramétrer pour qu'il démarre seul quand le système boot
    ```
    [clem@front ~]$ sudo systemctl enable nginx
    Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
    ```
- repérer le port qu'utilise NGINX par défaut, pour l'ouvrir dans le firewall
    ```
    [clem@front ~]$ sudo ss -alnpt
    State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port        Process
    LISTEN        0             128                        0.0.0.0:80                      0.0.0.0:*            users:(("nginx",pid=4209,fd=8),("nginx",pid=4208,fd=8))
    LISTEN        0             128                           [::]:80                         [::]:*            users:(("nginx",pid=4209,fd=9),("nginx",pid=4208,fd=9))

    [clem@front ~]$ sudo firewall-cmd --add-port=80/tcp
    success
    [clem@front ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
    success
    [clem@front ~]$ sudo firewall-cmd --list-all
    public (active)
      target: default
      icmp-block-inversion: no
      interfaces: enp0s3 enp0s8
      sources:
      services: ssh
      ports: 80/tcp
      protocols:
      masquerade: no
      forward-ports:
      source-ports:
      icmp-blocks:
      rich rules:
    ```
- vérifier que vous pouvez joindre NGINX avec une commande curl depuis votre PC
    ```
    PS C:\Users\cleme> curl 10.102.1.14                                                                        

    StatusCode        : 200
    StatusDescription : OK
    Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
                        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

                        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
                          <head>
                            <title>Test Page for the Nginx...
    [...]
    ```
    
**🌞 Explorer la conf par défaut de NGINX**

- repérez l'utilisateur qu'utilise NGINX par défaut
    `user nginx;`
- dans la conf NGINX, on utilise le mot-clé server pour ajouter un nouveau site
    - repérez le bloc server {} dans le fichier de conf principal
        ```
        server {
                listen       80 default_server;
                listen       [::]:80 default_server;
                server_name  _;
                root         /usr/share/nginx/html;

                # Load configuration files for the default server block.
                include /etc/nginx/default.d/*.conf;

                location / {
                }

                error_page 404 /404.html;
                    location = /40x.html {
                }

                error_page 500 502 503 504 /50x.html;
                    location = /50x.html {
                }
            }
        ```
- par défaut, le fichier de conf principal inclut d'autres fichiers de conf
    - mettez en évidence ces lignes d'inclusion dans le fichier de conf principal
        ```
        include /usr/share/nginx/modules/*.conf;
        include             /etc/nginx/mime.types;
        include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/default.d/*.conf;
        ```

**🌞 Modifier la conf de NGINX**

- pour que ça fonctionne, le fichier /etc/hosts de la machine DOIT être rempli correctement, conformément à la 📝checklist📝
    ```
    [clem@front ~]$ cat /etc/hosts
    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
    10.102.1.11 web web.tp2.linux
    10.102.1.12 db db.tp2.linux
    10.102.1.13 backup backup.tp2.linux
    10.102.1.14 front front.tp2.linux
    ```
- supprimer le bloc server {} par défaut, pour ne plus présenter la page d'accueil NGINX
    ```
    [clem@front ~]$ sudo cat /etc/nginx/nginx.conf
    # For more information on configuration, see:
    #   * Official English Documentation: http://nginx.org/en/docs/
    #   * Official Russian Documentation: http://nginx.org/ru/docs/

    user nginx;
    worker_processes auto;
    error_log /var/log/nginx/error.log;
    pid /run/nginx.pid;

    # Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
    include /usr/share/nginx/modules/*.conf;

    events {
        worker_connections 1024;
    }

    http {
        log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent" "$http_x_forwarded_for"';

        access_log  /var/log/nginx/access.log  main;

        sendfile            on;
        tcp_nopush          on;
        tcp_nodelay         on;
        keepalive_timeout   65;
        types_hash_max_size 2048;

        include             /etc/nginx/mime.types;
        default_type        application/octet-stream;

        # Load modular configuration files from the /etc/nginx/conf.d directory.
        # See http://nginx.org/en/docs/ngx_core_module.html#include
        # for more information.
        include /etc/nginx/conf.d/*.conf;

    # Settings for a TLS enabled server.
    #
    #    server {
    #        listen       443 ssl http2 default_server;
    #        listen       [::]:443 ssl http2 default_server;
    #        server_name  _;
    #        root         /usr/share/nginx/html;
    #
    #        ssl_certificate "/etc/pki/nginx/server.crt";
    #        ssl_certificate_key "/etc/pki/nginx/private/server.key";
    #        ssl_session_cache shared:SSL:1m;
    #        ssl_session_timeout  10m;
    #        ssl_ciphers PROFILE=SYSTEM;
    #        ssl_prefer_server_ciphers on;
    #
    #        # Load configuration files for the default server block.
    #        include /etc/nginx/default.d/*.conf;
    #
    #        location / {
    #        }
    #
    #        error_page 404 /404.html;
    #            location = /40x.html {
    #        }
    #
    #        error_page 500 502 503 504 /50x.html;
    #            location = /50x.html {
    #        }
    #    }

    }
    ```
- créer un fichier /etc/nginx/conf.d/web.tp2.linux.conf avec le contenu suivant :
    - j'ai sur-commenté pour vous expliquer les lignes, n'hésitez pas à dégommer mes lignes de commentaires
        ```
        [clem@front ~]$ cat /etc/nginx/conf.d/web.tp2.linux.conf
        server {
                listen 80;
                server_name web.tp2.linux;
                location / {
                        proxy_pass http://web.tp2.linux;
                }
        }
        ```
        
## IV. Firewalling

### 2. Mise en place

#### A. Base de données

**🌞 Restreindre l'accès à la base de données db.tp2.linux**

```
[clem@db ~]$ sudo firewall-cmd --list-all
[sudo] password for clem:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
[clem@db ~]$ sudo firewall-cmd --set-default-zone=drop
success

[clem@db ~]$ sudo firewall-cmd --reload
success

[clem@db ~]$ sudo firewall-cmd --list-all
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
[clem@db ~]$ sudo firewall-cmd --zone=drop  --add-interface=enp0s8
Warning: ZONE_ALREADY_SET: 'enp0s8' already bound to 'drop'
success

[clem@db ~]$ sudo firewall-cmd --zone=drop  --add-interface=enp0s8 --permanent
The interface is under control of NetworkManager, setting zone to 'drop'.
success

[clem@db ~]$ sudo firewall-cmd --list-all
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

- seul le serveur Web doit pouvoir joindre la base de données sur le port 3306/tcp
    ```
    [clem@db ~]$ sudo firewall-cmd --new-zone=db --permanent
    success
    [clem@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11/32 --permanent
    success
    [clem@db ~]$ sudo firewall-cmd --zone=db --add-port=3306/tcp --permanent
    success
    ```
- vous devez aussi autoriser votre accès SSH
    ```
    [clem@db ~]$ sudo firewall-cmd --new-zone=ssh --permanent
    success
    [clem@db ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
    success
    [clem@db ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
    success
    ```
- n'hésitez pas à multiplier les zones (une zone ssh et une zone db par exemple)




**🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd**

- `sudo firewall-cmd --get-active-zones`
    ```
    [clem@db ~]$ sudo firewall-cmd --get-active-zones
    db
      sources: 10.102.1.11/32
    drop
      interfaces: enp0s8 enp0s3
    ssh
      sources: 10.102.1.1/32
    ```
- `sudo firewall-cmd --get-default-zone`
    ```
    [clem@db ~]$ sudo firewall-cmd --get-default-zone
    drop
    ```
- `sudo firewall-cmd --list-all --zone=?`
    ```
    [clem@db ~]$ sudo firewall-cmd --list-all --zone=db
    db (active)
      target: default
      icmp-block-inversion: no
      interfaces:
      sources: 10.102.1.11/32
      services:
      ports: 3306/tcp
      protocols:
      masquerade: no
      forward-ports:
      source-ports:
      icmp-blocks:
      rich rules:

    [clem@db ~]$ sudo firewall-cmd --list-all --zone=ssh
    ssh (active)
      target: default
      icmp-block-inversion: no
      interfaces:
      sources: 10.102.1.1/32
      services:
      ports: 22/tcp
      protocols:
      masquerade: no
      forward-ports:
      source-ports:
      icmp-blocks:
      rich rules:
    ```


#### B. Serveur Web

**🌞 Restreindre l'accès au serveur Web web.tp2.linux**

```
[clem@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[clem@web ~]$ sudo firewall-cmd --set-default-zone=drop
success
[clem@web ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8
Warning: ZONE_ALREADY_SET: 'enp0s8' already bound to 'drop'
success
```

- seul le reverse proxy front.tp2.linux doit accéder au serveur web sur le port 80
    ```
    [clem@web ~]$ sudo firewall-cmd --new-zone=proxy --permanent
    success
    [clem@web ~]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.14/32 --permanent
    success
    [clem@web ~]$ sudo firewall-cmd --zone=proxy --add-port=80/tcp --permanent
    success
    ```
- n'oubliez pas votre accès SSH
    ```
    [clem@web ~]$ sudo firewall-cmd --new-zone=ssh --permanent
    success
    [clem@web ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
    success
    [clem@web ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
    success
    ```

**🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd**

```
[clem@web ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.14/32
ssh
  sources: 10.102.1.1/32
  
[clem@web ~]$ sudo firewall-cmd --get-default-zone
drop

[clem@web ~]$ sudo firewall-cmd --list-all --zone=proxy
proxy (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.14/32
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
[clem@web ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

#### C. Serveur de backup

**🌞 Restreindre l'accès au serveur de backup backup.tp2.linux**

```
[clem@backup ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: nfs ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[clem@backup ~]$ sudo firewall-cmd --set-default-zone=drop
success
[clem@backup ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 --permanent
The interface is under control of NetworkManager, setting zone to 'drop'.
success
```

- seules les machines qui effectuent des backups doivent être autorisées à contacter le serveur de backup via NFS
    ```
    [clem@backup ~]$ sudo firewall-cmd --new-zone=nfs --permanent
    success
    [clem@backup ~]$ sudo firewall-cmd --zone=nfs --add-source=10.102.1.11/32 --permanent
    success
    [clem@backup ~]$ sudo firewall-cmd --zone=nfs --add-source=10.102.1.12/32 --permanent
    success
    [clem@backup ~]$ sudo firewall-cmd --zone=nfs --add-port=2049/tcp --permanent
    success
    ```
- n'oubliez pas votre accès SSH
    ```
    [clem@backup ~]$ sudo firewall-cmd --new-zone=ssh --permanent
    success
    [clem@backup ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
    success
    [clem@backup ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
    success
    ```

**🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd**

```
[clem@backup ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
nfs
  sources: 10.102.1.11/32 10.102.1.12/32
ssh
  sources: 10.102.1.1/32
  
[clem@backup ~]$ sudo firewall-cmd --get-default-zone
drop

[clem@backup ~]$ sudo firewall-cmd --list-all --zone=nfs
nfs (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32 10.102.1.12/32
  services:
  ports: 2049/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
[clem@backup ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

#### D. Reverse Proxy

**🌞 Restreindre l'accès au reverse proxy front.tp2.linux**

```
[clem@front ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[clem@front ~]$ sudo firewall-cmd --set-default-zone=drop
success
[clem@front ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 --permanent
The interface is under control of NetworkManager, setting zone to 'drop'.
success
```

- seules les machines du réseau 10.102.1.0/24 doivent pouvoir joindre le proxy
    ```
    [clem@front ~]$ sudo firewall-cmd --new-zone=reseau --permanent
    success
    [clem@front ~]$ sudo firewall-cmd --zone=reseau --add-source=10.102.1.0/24 --permanent
    success
    [clem@front ~]$ sudo firewall-cmd --zone=reseau --add-port=80/tcp --permanent
    success
    ```
- n'oubliez pas votre accès SSH
    ```
    [clem@front ~]$ sudo firewall-cmd --new-zone=ssh --permanent
    success
    [clem@front ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
    success
    [clem@front ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
    success
    ```

**🌞 Montrez le résultat de votre conf avec une ou plusieurs commandes firewall-cmd**

```
[clem@front ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
reseau
  sources: 10.102.1.0/24
ssh
  sources: 10.102.1.1/32
  
[clem@front ~]$ sudo firewall-cmd --get-default-zone
drop

[clem@front ~]$ sudo firewall-cmd --list-all --zone=reseau
reseau (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.0/24
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
[clem@front ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

#### E. Tableau récap

**🌞 Rendez-moi le tableau suivant, correctement rempli :**

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées                  |
|--------------------|---------------|-------------------------|-------------|---------------------------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | 80/tcp      | 10.102.1.14/32                  |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | 3306/tcp    | 10.102.1.11/32                  |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | 2049/tcp    | 10.102.1.11/32 & 10.102.1.12/32 |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | 80/tcp      | 10.102.1.0/24                   |
