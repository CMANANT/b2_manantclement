#!/bin/bash
# Simple backup script
# clementmanant ~ 15/12/21

here=$1
tobackup=$2

if [ -d "$here" ]; then
	file="tp2_backup_$(date '+%y-%m-%d_%H-%M-%S').tar.gz"
	mysqldump --user=nextcloud --password=meow --${tobackup} > backup_donnees.sql
	tar -czf "$file" backup_donnees.sql
	rsync -av $file $here
	echo "Archive successfully created."
	else
	echo "ATTENTION: Le dossier de destination n'existe pas: $here"
fi
