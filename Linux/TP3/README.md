# TP3 : Your own shiet : Création d'un serveur VPN

## Sommaire

- [TP3 : Your own shiet : Création d'un serveur VPN](#tp3--your-own-shiet--création-dun-serveur-vpn)
- [Sommaire](#sommaire)
- [Prérequis](#prérequis)
- [I. Mise en place du VPN](#i-mise-en-place-du-vpn)
    - [Installer Wireguard et générer une paire de clé ssh sans script](#installer-wireguard-et-générer-une-paire-de-clé-ssh-sans-script)
    - [Installer Wireguard et générer une paire de clé ssh avec script](#installer-wireguard-et-générer-une-paire-de-clé-ssh-avec-script)
    - [Configuration de Wireguard](#configuration-de-wireguard)
    - [Démarrage de Wireguard](#démarrage-de-wireguard)
- [II. Ajout d'un client](#ii-ajout-dun-client)
    - [Installer Wireguard et générer une paire de clé ssh sans script côté client](#installer-wireguard-et-générer-une-paire-de-clé-ssh-sans-script-côté-client)
    - [Installer Wireguard et générer une paire de clé ssh avec script côté client](#installer-wireguard-et-générer-une-paire-de-clé-ssh-avec-script-côté-client)
    - [Configuration de Wireguard côté client](#configuration-de-wireguard-côté-client)
    - [Démarrage de Wireguard côté client](#démarrage-de-wireguard-côté-client)
- [III. Création d'une backup](#iii-création-dune-backup)
    - [Sur la machine de backup](#sur-la-machine-de-backup)
    - [Sur la machine à backup](#sur-la-machine-à-backup)
    - [Script de backup](#script-de-backup)
    - [Unité de service](#unité-de-service)
- [IV. Monitoring](#iv-monitoring)
    - [Installation](#installation)
    - [Alertes](#alertes)
    - [Aperçu](#aperçu)


## Prérequis

- 2 VM Rocky Linux fonctionnelle (+ les clients)

| Machine            | IP            | IP sur le VPN (wg0)     | Service     |
|--------------------|---------------|-------------------------|-------------|
| `vpn.tp3.linux`    | `10.103.1.2`  | `10.10.3.1`             | Serveur VPN |
| `client.tp3.linux` | `10.102.1.3`  | `10.10.3.2`             | Client VPN  |
| `backup.tp3.linux` | `10.102.1.4`  | X                       | Backup      |


## I. Mise en place du VPN

### **Installer Wireguard et générer une paire de clé ssh sans script** 

**Mise à jour et installation des paquets nécessaires.**

```
[clem@vpn ~]$ sudo dnf update -y
[clem@vpn ~]$ sudo dnf install -y elrepo-release epel-release
[clem@vpn ~]$ sudo dnf install -y kmod-wireguard wireguard-tools
```

**On change le propriétaire du fichier**

`[clem@vpn ~]$ sudo chown clem /etc/wireguard/`


**Génération d'une paire de clé ssh.**

```
[clem@vpn ~]$ wg genkey | tee /etc/wireguard/wireguard.key | wg pubkey > /etc/wireguard/wireguard.pub.key

# On change les permissions de la clé privée
[clem@vpn ~]$ sudo chmod 600 /etc/wireguard/wireguard.key
[clem@vpn ~]$ ls -al /etc/wireguard/
total 20
drwx------.  2 clem root   52 Dec 21 18:04 .
drwxr-xr-x. 92 root root 8192 Dec 21 18:03 ..
-rw-------.  1 clem clem   45 Dec 21 18:04 wireguard.key
-rw-rw-r--.  1 clem clem   45 Dec 21 18:04 wireguard.pub.key

# Pour afficher les clés publiques et privées
[clem@vpn ~]$ cat /etc/wireguard/wireguard.key
qDwwIyiM/McV9jibL6tFAezbBvxFsLRPCcm+vRKl2nQ=
[clem@vpn ~]$ cat /etc/wireguard/wireguard.pub.key
ZO4WptKaTt2B1zqVhITNZz3/EsW6/67p4PlUXnOYHEY=
```

### **Installer Wireguard et générer une paire de clé ssh avec script** 
Lancez le script d'installation ![**`install_wireguard.sh`**](./Scripts/install_wireguard.sh)

N'oubliez pas d'entrer votre nom d'utilisateur.
`[clem@localhost ~]$ sudo bash ./install_wireguard.sh clem`


### **Configuration de Wireguard** 
**Création du fichier de conf**

```
[clem@vpn ~]$ sudo vim /etc/wireguard/wg0.conf
[Interface]
Address = 10.10.3.1/24
SaveConfig = true
PostUp = firewall-cmd --add-port=51820/udp; firewall-cmd --zone=public --add-masquerade; firewall-cmd --direct --add-rule ipv4 filter FORWARD 0 -i wg0 -o enp0s8 -j ACCEPT; firewall-cmd --direct --add-rule ipv4 nat POSTROUTING 0 -o enp0s8 -j MASQUERADE
PostDown = firewall-cmd --remove-port=51820/udp; firewall-cmd --zone=public --remove-masquerade; firewall-cmd --direct --remove-rule ipv4 filter FORWARD 0 -i wg0 -o enp0s8 -j ACCEPT; firewall-cmd --direct --remove-rule ipv4 nat POSTROUTING 0-o enp0s8 -j MASQUERADE
ListenPort = 51820
PrivateKey = qDwwIyiM/McV9jibL6tFAezbBvxFsLRPCcm+vRKl2nQ=
```

**📁 Contenu du fichier de conf ![`wg0.conf`](./Conf/wg0_vpn.conf)**

**Mise en place de l'IP Forwarding**

```
[clem@vpn ~]$ echo "net.ipv4.ip_forward = 1" | sudo tee -a /etc/sysctl.conf
net.ipv4.ip_forward = 1
[clem@vpn ~]$ sudo sysctl -p
net.ipv4.ip_forward = 1
```

### **Démarrage de Wireguard** 
**Démarrage du serveur**

```
[clem@vpn ~]$ sudo systemctl start wg-quick@wg0

# Lancement au démarrage
[clem@vpn ~]$ sudo systemctl enable wg-quick@wg0
Created symlink /etc/systemd/system/multi-user.target.wants/wg-quick@wg0.service → /usr/lib/systemd/system/wg-quick@.service.

# Vérification
[clem@vpn ~]$ sudo systemctl status wg-quick@wg0
● wg-quick@wg0.service - WireGuard via wg-quick(8) for wg0
   Loaded: loaded (/usr/lib/systemd/system/wg-quick@.service; enabled; vendor preset: disabled)
   Active: active (exited) since Tue 2021-12-21 18:09:20 CET; 27s ago
     Docs: man:wg-quick(8)
           man:wg(8)
           https://www.wireguard.com/
           https://www.wireguard.com/quickstart/
           https://git.zx2c4.com/wireguard-tools/about/src/man/wg-quick.8
           https://git.zx2c4.com/wireguard-tools/about/src/man/wg.8
 Main PID: 5376 (code=exited, status=0/SUCCESS)
    Tasks: 0 (limit: 4946)
   Memory: 0B
   CGroup: /system.slice/system-wg\x2dquick.slice/wg-quick@wg0.service

Dec 21 18:09:18 vpn.tp3.linux wg-quick[5376]: [#] ip link add wg0 type wireguard
Dec 21 18:09:18 vpn.tp3.linux wg-quick[5376]: [#] wg setconf wg0 /dev/fd/63
Dec 21 18:09:18 vpn.tp3.linux wg-quick[5376]: [#] ip -4 address add 10.10.3.1/24 dev wg0
Dec 21 18:09:18 vpn.tp3.linux wg-quick[5376]: [#] ip link set mtu 1420 up dev wg0
Dec 21 18:09:18 vpn.tp3.linux wg-quick[5376]: [#] firewall-cmd --add-port=51820/udp; firewall-cmd --zone=pub>
Dec 21 18:09:18 vpn.tp3.linux wg-quick[5376]: success
Dec 21 18:09:19 vpn.tp3.linux wg-quick[5376]: success
Dec 21 18:09:19 vpn.tp3.linux wg-quick[5376]: success
Dec 21 18:09:20 vpn.tp3.linux wg-quick[5376]: success
Dec 21 18:09:20 vpn.tp3.linux systemd[1]: Started WireGuard via wg-quick(8) for wg0.
```

Il faudra peut-être mettre les paquets à jour et redémarrer la machine en cas d'erreur :
```
[clem@vpn ~]$ sudo dnf update -y
[clem@vpn ~]$ sudo reboot
```

## II. Ajout d'un client

### **Installer Wireguard et générer une paire de clé ssh sans script côté client** 

**Mise à jour et installation des paquets nécessaires.**

```
[clem@vpn ~]$ sudo dnf update -y
[clem@vpn ~]$ sudo dnf install -y elrepo-release epel-release
[clem@vpn ~]$ sudo dnf install -y kmod-wireguard wireguard-tools
```

**On change le propriétaire du fichier**

`[clem@vpn ~]$ sudo chown clem /etc/wireguard/`


**Génération d'une paire de clé ssh.**

```
[clem@vpn ~]$ wg genkey | tee /etc/wireguard/wireguard.key | wg pubkey > /etc/wireguard/wireguard.pub.key

# On change les permissions de la clé privée
[clem@vpn ~]$ sudo chmod 600 /etc/wireguard/wireguard.key
[clem@client ~]$ ls -al /etc/wireguard/
total 20
drwx------.  2 clem root   52 Dec 21 18:37 .
drwxr-xr-x. 92 root root 8192 Dec 21 18:36 ..
-rw-------.  1 clem clem   45 Dec 21 18:37 wireguard.key
-rw-rw-r--.  1 clem clem   45 Dec 21 18:37 wireguard.pub.key

# Pour afficher les clés publiques et privées
[clem@client ~]$ cat /etc/wireguard/wireguard.key
IInaR685hGDxcgtbsLBjA2bBwmdmd0C9HFzj8agM9mU=
[clem@client ~]$ cat /etc/wireguard/wireguard.pub.key
W0bbQ4KgghElKs3zrbXVk0pQeWke4cuCibyIhMT8CQE=
```

### **Installer Wireguard et générer une paire de clé ssh avec script côté client** 
Lancez le script d'installation ![**`install_wireguard.sh`**](./Scripts/install_wireguard.sh)

N'oubliez pas d'entrer votre nom d'utilisateur.
`[clem@localhost ~]$ sudo bash ./install_wireguard.sh clem`

### **Configuration de Wireguard côté client** 

**Création du fichier de conf client**

```
[clem@client ~]$ sudo vim /etc/wireguard/wg0.conf
[Interface]
PrivateKey = IInaR685hGDxcgtbsLBjA2bBwmdmd0C9HFzj8agM9mU=
Address = 10.10.3.2/24

[Peer]
PublicKey = ZO4WptKaTt2B1zqVhITNZz3/EsW6/67p4PlUXnOYHEY=
Endpoint = 10.103.1.2:51820
AllowedIPs = 0.0.0.0/0
```

**📁 Contenu du fichier de conf ![`wg0.conf`](./Conf/wg0_client.conf)**

**Modification de conf serveur**

Sur la machine serveur : 
`[clem@vpn ~]$ sudo wg set wg0 peer W0bbQ4KgghElKs3zrbXVk0pQeWke4cuCibyIhMT8CQE= allowed-ips 10.10.3.2`

### **Démarrage de Wireguard côté client** 

**Démarrage du service**

```
[clem@client ~]$ sudo systemctl start wg-quick@wg0

# Lancement au démarrage
[clem@client ~]$ sudo systemctl enable wg-quick@wg0
Created symlink /etc/systemd/system/multi-user.target.wants/wg-quick@wg0.service → /usr/lib/systemd/system/wg-quick@.service.

# Vérification
[clem@client ~]$ sudo systemctl status wg-quick@wg0
● wg-quick@wg0.service - WireGuard via wg-quick(8) for wg0
   Loaded: loaded (/usr/lib/systemd/system/wg-quick@.service; enabled; vendor preset: disabled)
   Active: active (exited) since Tue 2021-12-21 18:59:37 CET; 8s ago
     Docs: man:wg-quick(8)
           man:wg(8)
           https://www.wireguard.com/
           https://www.wireguard.com/quickstart/
           https://git.zx2c4.com/wireguard-tools/about/src/man/wg-quick.8
           https://git.zx2c4.com/wireguard-tools/about/src/man/wg.8
  Process: 5604 ExecStart=/usr/bin/wg-quick up wg0 (code=exited, status=0/SUCCESS)
 Main PID: 5604 (code=exited, status=0/SUCCESS)

Dec 21 18:59:37 client.tp3.linux wg-quick[5604]: [#] wg setconf wg0 /dev/fd/63
Dec 21 18:59:37 client.tp3.linux wg-quick[5604]: [#] ip -4 address add 10.10.3.2/24 dev wg0
Dec 21 18:59:37 client.tp3.linux wg-quick[5604]: [#] ip link set mtu 1420 up dev wg0
Dec 21 18:59:37 client.tp3.linux wg-quick[5604]: [#] wg set wg0 fwmark 51820
Dec 21 18:59:37 client.tp3.linux wg-quick[5604]: [#] ip -4 route add 0.0.0.0/0 dev wg0 table 51820
Dec 21 18:59:37 client.tp3.linux wg-quick[5604]: [#] ip -4 rule add not fwmark 51820 table 51820
Dec 21 18:59:37 client.tp3.linux wg-quick[5604]: [#] ip -4 rule add table main suppress_prefixlength 0
Dec 21 18:59:37 client.tp3.linux wg-quick[5604]: [#] sysctl -q net.ipv4.conf.all.src_valid_mark=1
Dec 21 18:59:37 client.tp3.linux wg-quick[5604]: [#] nft -f /dev/fd/63
Dec 21 18:59:37 client.tp3.linux systemd[1]: Started WireGuard via wg-quick(8) for wg0.
```

**Vérifications**
Vous pouvez à présent retirer la carte NAT pour faire quelques vérifications.

```
# Le client de ping le serveur, ils sont bien connectés.
[clem@client ~]$ ping -c 2 10.10.3.2
PING 10.10.3.2 (10.10.3.2) 56(84) bytes of data.
64 bytes from 10.10.3.2: icmp_seq=1 ttl=64 time=0.055 ms
64 bytes from 10.10.3.2: icmp_seq=2 ttl=64 time=0.145 ms

--- 10.10.3.2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1009ms
rtt min/avg/max/mdev = 0.055/0.100/0.145/0.045 ms

# Le client peut ping internet avec de la résolution de nom sans carte NAT.
[clem@client ~]$ ping -c 2 google.com
PING google.com (172.217.18.206) 56(84) bytes of data.
64 bytes from ham02s14-in-f206.1e100.net (172.217.18.206): icmp_seq=1 ttl=114 time=14.9 ms
64 bytes from ham02s14-in-f206.1e100.net (172.217.18.206): icmp_seq=2 ttl=114 time=14.1 ms

--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 14.110/14.520/14.930/0.410 ms
```

## III. Création d'une backup

***A faire pour chaque machine à backup :***

### **Sur la machine de backup**

**Installation des paquets nécessaires**
`[clem@backup ~]$ sudo dnf install -y nfs-utils`

**Création du dossier de backup**
`[clem@backup ~]$ sudo mkdir -p /srv/backups/vpn.tp3.linux/`

**Configuration du serveur nfs**
```
[clem@backup ~]$ sudo vim /etc/idmapd.conf
[clem@backup ~]$ sudo cat /etc/idmapd.conf | grep tp3
Domain = tp3.linux
[clem@backup ~]$ sudo vim /etc/exports
[clem@backup ~]$ sudo cat /etc/exports
/srv/backups/vpn.tp3.linux 10.103.1.2(rw,no_root_squash)
```

**Démarrage du service**
```
[clem@backup ~]$ sudo systemctl start nfs-server
[clem@backup ~]$ sudo systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
```

**Gestion du firewall**
```
[clem@backup ~]$ sudo firewall-cmd --add-service=nfs --permanent
success
[clem@backup ~]$ sudo firewall-cmd --reload
success
[clem@backup ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: nfs ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
  
### **Sur la machine à backup**

**Installation des paquets nécessaires** 
`[clem@vpn ~]$ sudo dnf install -y nfs-utils`

**Création du dossier à backup**
`[clem@vpn ~]$ sudo mkdir -p /srv/backup/`

**Configuration du partage NFS**
```
[clem@vpn ~]$ sudo vim /etc/idmapd.conf
[clem@vpn ~]$ sudo cat /etc/idmapd.conf | grep tp3
Domain = tp3.linux

[clem@vpn ~]$ sudo mount -t nfs 10.103.1.4:/srv/backups/vpn.tp3.linux /srv/backup/
[clem@vpn ~]$ sudo df -hT | grep tp3
10.103.1.4:/srv/backups/vpn.tp3.linux nfs4      6.2G  2.1G  4.2G  34% /srv/backup

[clem@vpn ~]$ sudo vim /etc/fstab
[clem@vpn ~]$ sudo cat /etc/fstab | grep tp3
10.103.1.4:/srv/backups/vpn.tp3.linux /srv/backup nfs defaults 0 0
```


### **Script de backup**

```
[clem@vpn ~]$ sudo vim /srv/tp3_backup.sh
[clem@vpn ~]$ sudo cat /srv/tp3_backup.sh
#!/bin/bash
# Simple backup script
# clementmanant ~ 21/12/21

here=$1
tobackup=$2

if [ -z "$tobackup" ]; then
        echo "Il faut 2 dossiers en argument"
        exit 0
fi

if [ -d "$here" ]; then
        file="wireguard_$(date '+%y-%m-%d_%H-%M-%S').tar.gz"
        tar -czf "$file" "$tobackup"
        rsync -av $file $here
        echo "Archive successfully created."
        else
        echo "Le dossier de destination n'existe pas: $here"
fi
```

**📁 Contenu du script de backup ![`tp3_backup.sh`](./Scripts/tp3_backup.sh)**

### **Unité de service**

**Création de l'unité de service**
```
[clem@vpn ~]$ sudo vim /etc/systemd/system/tp3_backup.service
[clem@vpn ~]$ sudo cat /etc/systemd/system/tp3_backup.service
[Unit]
Description=Backup service TP3

[Service]
ExecStart=sudo bash /srv/tp3_backup.sh /srv/backup/ /etc/wireguard/
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

**📁 Contenu de l'unité de service ![`tp3_backup.service`](./Unite_de_service/tp3_backup.service)**

**Création d'un timer**
```
[clem@vpn ~]$ sudo vim /etc/systemd/system/tp3_backup.timer
[clem@vpn ~]$ sudo cat /etc/systemd/system/tp3_backup.timer
[Unit]
Description=Run script backup TP3
Requires=tp3_backup.service

[Timer]
Unit=tp3_backup.service
OnCalendar=*-*-* 13:00:00

[Install]
WantedBy=timers.target
```

**📁 Contenu du timer ![`tp3_backup.timer`](./Unite_de_service/tp3_backup.timer)**

**Lancement du timer**
```
[clem@vpn ~]$ sudo systemctl daemon-reload
[clem@vpn ~]$ sudo systemctl start tp3_backup.timer
[clem@vpn ~]$ sudo systemctl enable tp3_backup.timer
Created symlink /etc/systemd/system/timers.target.wants/tp3_backup.timer → /etc/systemd/system/tp3_backup.timer.
```

## IV. Monitoring

**A faire pour chaque machine à monitorer :**

### **Installation**

**Installation de netdata**
```
[clem@vpn ~]$ sudo su -
[root@vpn ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
[root@vpn ~]# exit
logout
```

**Vérification**
```
[clem@vpn ~]$ sudo systemctl is-active netdata
active
[clem@vpn ~]$ sudo systemctl is-enabled netdata
enabled
```

**Déterminer le port de netdata**

```
[clem@vpn ~]$ sudo ss -alnpt | grep netdata
LISTEN 0      128        127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=7703,fd=42))
LISTEN 0      128          0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=7703,fd=5))
LISTEN 0      128            [::1]:8125          [::]:*    users:(("netdata",pid=7703,fd=41))
LISTEN 0      128             [::]:19999         [::]:*    users:(("netdata",pid=7703,fd=6))
```

**Autoriser le port**

```
[clem@vpn ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[clem@vpn ~]$ sudo firewall-cmd --reload
success
[clem@vpn ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 19999/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

### **Alertes**

**Setup alerting**

```
[clem@vpn ~]$ sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf
[clem@vpn ~]$ sudo cat /opt/netdata/etc/netdata/health_alarm_notify.conf | grep DISCORD
SEND_DISCORD="YES"
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897130330086047755/fD_Ly5iDII_KmGD-tuZSoKgvtSijqQIyKWE8tFF7BorTpXiTNUmCNtmlRh1MtHYmHdAr"
DEFAULT_RECIPIENT_DISCORD="alert_clement"
[...]
```

**Test**

```
[clem@vpn ~]$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2021-12-22 16:23:24: alarm-notify.sh: INFO: sent discord notification for: vpn.tp3.linux test.chart.test_alarm is WARNING to 'alert_clement'
# OK

# SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
2021-12-22 16:23:25: alarm-notify.sh: INFO: sent discord notification for: vpn.tp3.linux test.chart.test_alarm is CRITICAL to 'alert_clement'
# OK

# SENDING TEST CLEAR ALARM TO ROLE: sysadmin
2021-12-22 16:23:25: alarm-notify.sh: INFO: sent discord notification for: vpn.tp3.linux test.chart.test_alarm is CLEAR to 'alert_clement'
# OK
```

**Configuration alarme RAM**

```
[clem@vpn ~]$ sudo /opt/netdata/etc/netdata/edit-config health.d/ram.conf
[...]
warn: $this > (($status >= $WARNING)  ? (70) : (80))
crit: $this > (($status == $CRITICAL) ? (80) : (98))
[...]
```

**Configuration alarme CPU**

```
[clem@vpn ~]$ sudo /opt/netdata/etc/netdata/edit-config health.d/cpu.conf
[...]
warn: $this > (($status >= $WARNING)  ? (75) : (85))
crit: $this > (($status == $CRITICAL) ? (85) : (95))
[...]
```

### **Aperçu**

Sur votre navigateur web : 
`10.103.1.2:19999`

![](./Images/Netdata.PNG)

