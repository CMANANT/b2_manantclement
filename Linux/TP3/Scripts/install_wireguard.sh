#!/bin/bash
# Install Wireguard
# clementmanant ~ 22/12/2021

name=$1

if [ -z "$name" ]
then
	echo "Entrez votre nom d'utilisateur"
else
	dnf update -y
	dnf install -y elrepo-release epel-release
	dnf install -y kmod-wireguard wireguard-tools
	chown $name /etc/wireguard/
	wg genkey | tee /etc/wireguard/wireguard.key | wg pubkey > /etc/wireguard/wireguard.pub.key
	chown $name /etc/wireguard/wireguard.key
	chown $name /etc/wireguard/wireguard.pub.key
	chmod 600 /etc/wireguard/wireguard.key
	ls -al /etc/wireguard/
fi
