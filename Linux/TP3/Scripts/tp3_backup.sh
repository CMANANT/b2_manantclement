#!/bin/bash
# Simple backup script
# clementmanant ~ 21/12/21

here=$1
tobackup=$2

if [ -z "$tobackup" ]; then
	echo "Il faut 2 dossiers en argument"
	exit 0
fi

if [ -d "$here" ]; then
	file="wireguard_$(date '+%y-%m-%d_%H-%M-%S').tar.gz"
	tar -czf "$file" "$tobackup"
	rsync -av $file $here
	echo "Archive successfully created."
	else
	echo "Le dossier de destination n'existe pas: $here"
fi
