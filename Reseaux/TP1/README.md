# TP1 - Mise en jambes

## I. Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

**🌞 Affichez les infos des cartes réseau de votre PC :** 

*Interface Wi-Fi :* 

`ipconfig / all`

```
Carte réseau sans fil Wi-Fi 2 :

   Adresse physique . . . . . . . . . . . : A0-A4-C5-41-BF-4F
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.1.69
```

*Interface Ethernet :* 

Je n'ai pas de port Ethernet.

**🌞 Affichez votre gateway**

`ipconfig`

```
Carte réseau sans fil Wi-Fi 2 :

   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```

### En graphique (GUI : Graphical User Interface)

**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

Dans les paramètres du PC -> Réseau et internet -> Etat -> Afficher vos propriétés réseau.

![](Reseaux/TP1/Images/CarteIP.PNG)

### Questions

🌞 à quoi sert la gateway dans le réseau d'YNOV ?

La gateway dans le réseau d'YNOV permet de se connecter à des réseaux extérieurs.

### 2. Modifications des informations

### A. Modification d'adresse IP (part 1)

**🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP :**

Dans les paramètres du PC -> Réseau et internet -> Etat -> Modifier les propriétés de connexion -> Dans l'onglet Paramètres IP -> Modifier.

![](Reseaux/TP1/Images/ChangeIP.PNG)

**🌞 Il est possible que vous perdiez l'accès à internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.**

En changeant son adresse IP, il est possible d'en choisir une déjà utilisée, dans ce cas la réception de données est impossible.

### B. Table ARP

**🌞 Exploration de la table ARP**

Pour afficher la table ARP : 
`arp /a`

Adresse MAC : 
`10.33.3.253           00-12-00-40-4c-bf     dynamique` 

Il s'agit de la dernière adresse du réseau.

**🌞 Et si on remplissait un peu la table ?**

```
ping 10.33.2.222
ping 10.33.3.219
ping 10.33.3.232
```

![](Reseaux/TP1/Images/arp1.PNG)

### C. nmap

**🌞 Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre**

Scan ping Ynov : 

![](Reseaux/TP1/Images/scanping.PNG)

Table ARP : 

![](Reseaux/TP1/Images/arp2.PNG)

### D. Modification d'adresse IP (part 2)
**🌞 Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à nmap.**
![](Reseaux/TP1/Images/modifIP.PNG)

`nmap -sP 10.33.0.0/22`

![](Reseaux/TP1/Images/pinggoogle.PNG)

## II. Exploration locale en duo

### 3. Modification d'adresse IP

**🌞 Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :**

```
ping 192.168.0.5

Envoi d’une requête 'Ping'  192.168.0.5 avec 32 octets de données :
Réponse de 192.168.0.5 : octets=32 temps<1ms TTL=128
Réponse de 192.168.0.5 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.5 : octets=32 temps=1 ms TTL=128
```

### 4. Utilisation d'un des deux comme gateway

**🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu**

```
ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=20 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=19 ms TTL=57
```




**🌞 utiliser un traceroute ou tracert pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)**

```
tracert 192.168.0.5

Détermination de l’itinéraire vers DESKTOP-DM8M0FQ [192.168.0.5]
avec un maximum de 30 sauts :

  1    <1 ms    <1 ms    <1 ms  DESKTOP-DM8M0FQ [192.168.0.5]

Itinéraire déterminé.
```

### 5. Petit chat privé

**🌞 sur le PC serveur**

![](Reseaux/TP1/Images/chatpv1.PNG)

**🌞 sur le PC client**

`.\nc.exe 192.168.43.88 8888`

### 6. Firewall

**🌞 Autoriser les ping**

![](Reseaux/TP1/Images/firewallping.PNG)

**🌞 Autoriser le traffic sur le port qu'utilise nc**

![](Reseaux/TP1/Images/chatpv2.PNG)

## III. Manipulations d'autres outils/protocoles côté client
### 1. DHCP 
**🌞 Exploration du DHCP, depuis votre PC**

`ipconfig /all`

![](Reseaux/TP1/Images/DHCP.PNG)

Date d'expiration du bail : 

`jeudi 16 septembre 2021 19:18:50`

### 2. DNS

`ipconfig /all`

**🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur**

`Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8`

**🌞 utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main**

Google: 

![](Reseaux/TP1/Images/dnsgoogle.PNG)

Ynov :

![](Reseaux/TP1/Images/dnsynov.PNG)

Déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes : 
`8.8.8.8`

![](Reseaux/TP1/Images/reverselookup.PNG)

# IV. Wireshark
**🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet.**

Ping passerelle : 

![](Reseaux/TP1/Images/pingpasserelle.PNG)

Netcat : 

![](Reseaux/TP1/Images/selfping.PNG)

DNS : 

![](Reseaux/TP1/Images/dnsrequest.PNG)

Le serveur DNS de destination est 192.168.1.1.
