# TP2 : On va router des trucs

## I. ARP

### 1. Echange ARP

**🌞 Générer des requêtes ARP**

Effectuer un ping d'une machine à l'autre : 

```
[clem@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.729 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=1.04 ms
^C
--- 10.2.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1062ms
rtt min/avg/max/mdev = 0.729/0.886/1.043/0.157 ms
```

```
[clem@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.612 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=1.29 ms
^C
--- 10.2.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1008ms
rtt min/avg/max/mdev = 0.612/0.951/1.291/0.340 ms
```

Repérer l'adresse MAC de node1 dans la table ARP de node2 et vice-versa : 

```
[clem@node1 ~]$ ip n s
10.2.1.12 dev enp0s8 lladdr 08:00:27:98:47:69 STALE
```

```
[clem@node2 ~]$ ip n s
10.2.1.11 dev enp0s8 lladdr 08:00:27:49:0e:c0 STALE
```

Prouvez que l'info est correcte : 

```
[clem@node1 ~]$ ip n s
10.2.1.12 dev enp0s8 lladdr 08:00:27:98:47:69 STALE
```

```
[clem@node2 ~]$ ip a
...
link/ether 08:00:27:98:47:69
...
```

### 2. Analyse de trames

**🌞 Analyse de trames**

Videz vos tables ARP, sur les deux machines : 

```
[clem@node1 ~]$ sudo ip neigh flush all
[sudo] password for clem:
[clem@node1 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0d REACHABLE
```

```
[clem@node2 ~]$ sudo ip neigh flush all
[sudo] password for clem:
[clem@node2 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0d REACHABLE
```

Trames ARP : 

![](Reseaux/TP2/Images/trames_arp.PNG)

| ordre | type trame   | source                      | destination                   |
|-------|--------------|-----------------------------|-------------------------------|
| 1     | Requête ARP  | `node2` `08:00:27:98:47:69` | Broadcast `FF:FF:FF:FF:FF:FF` |
| 2     | Réponse ARP  | `node1` `08:00:27:49:0E:C0` | `node2` `08:00:27:98:47:69`   |
| 3     | Ping         | `node2` `10.2.1.12`         | `node1` `10.2.1.11`           |
| 4     | Pong         | `node1` `10.2.1.11`         | `node2` `10.2.1.12`           |

## II. Routage

```
[clem@router ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b1:e5:af brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85286sec preferred_lft 85286sec
    inet6 fe80::a00:27ff:feb1:e5af/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b7:dd:3f brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feb7:dd3f/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e8:42:d8 brd ff:ff:ff:ff:ff:ff
    inet 10.2.2.254/24 brd 10.2.2.255 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fee8:42d8/64 scope link
       valid_lft forever preferred_lft forever
[clem@router ~]$ hostname
router.net2.tp2
```

```
[clem@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:49:0e:c0 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.11/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe49:ec0/64 scope link
       valid_lft forever preferred_lft forever
[clem@node1 ~]$ hostname
node1.net1tp2
```

```
[clem@marcel ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a3:ff:28 brd ff:ff:ff:ff:ff:ff
    inet 10.2.2.12/24 brd 10.2.2.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fea3:ff28/64 scope link
       valid_lft forever preferred_lft forever
[clem@marcel ~]$ hostname
marcel.net2.tp2
```

### 1. Mise en place du routage

**🌞Activer le routage sur le noeud router.net2.tp2**

```
[clem@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[clem@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

**🌞 Ajouter les routes statiques nécessaires pour que node1.net1.tp2 et marcel.net2.tp2 puissent se ping**

```
[clem@node1 network-scripts]$ ip route show
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100
10.2.2.0/24 via 10.2.1.254 dev enp0s8 proto static metric 100
[clem@node1 network-scripts]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.23 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=2.47 ms
^C
--- 10.2.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 1.230/1.851/2.472/0.621 ms
```

```
[clem@marcel ~]$ ip route show
10.2.1.0/24 via 10.2.2.254 dev enp0s8 proto static metric 100
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
[clem@marcel ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=1.33 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=2.48 ms
^C
--- 10.2.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 1.331/1.903/2.475/0.572 ms
```

### 2. Analyse de trames

**🌞 Analyse des échanges ARP**

- videz les tables ARP des trois noeuds

```
[clem@node1 network-scripts]$ sudo ip neigh flush all
[sudo] password for clem:
[clem@node1 network-scripts]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0d REACHABLE
```

```
[clem@router ~]$ sudo ip neigh flush all
[sudo] password for clem:
[clem@router ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0d REACHABLE
```

```
[clem@marcel ~]$ sudo ip neigh flush all
[sudo] password for clem:
[clem@marcel ~]$ ip n s
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:4c REACHABLE
```

- effectuez un ping de node1.net1.tp2 vers marcel.net2.tp2

```
[clem@node1 /]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=2.40 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=2.34 ms
^C
--- 10.2.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 2.340/2.368/2.396/0.028 ms
```

- regardez les tables ARP des trois noeuds

```
[clem@node1 /]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0d DELAY
10.2.1.254 dev enp0s8 lladdr 08:00:27:b7:dd:3f STALE
```

```
[clem@router ~]$ ip n s
10.2.1.11 dev enp0s8 lladdr 08:00:27:49:0e:c0 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0d DELAY
10.2.2.12 dev enp0s9 lladdr 08:00:27:a3:ff:28 STALE
```

```
[clem@marcel ~]$ ip n s
10.2.2.254 dev enp0s8 lladdr 08:00:27:e8:42:d8 STALE
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:4c DELAY
```

- essayez de déduire un peu les échanges ARP qui ont eu lieu

Il devrait y avoir deux trames ARP dans chaque réseau, d'abord dans le premier réseau entre node1 et broadcast en request, puis de router à node1en reply, et dans le deuxième réseau de router à broadcast en request, puis de marcel à router en reply.

- répétez l'opération précédente (vider les tables, puis ping), en lançant tcpdump sur les 3 noeuds, afin de capturer les échanges depuis les 3 points de vue

![](Reseaux/TP2/Images/trames_arp_node1.PNG)

![](Reseaux/TP2/Images/trames_arp_marcel.PNG)

| ordre | type trame  | IP source | MAC source                   | IP destination | MAC destination               |
|-------|-------------|-----------|------------------------------|----------------|-------------------------------|
| 1     | Requête ARP | x         | `node1`  `08:00:27:49:0E:C0` | x              | Broadcast `FF:FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `router` `08:00:27:B7:DD:3F` | x              | `node1`   `08:00:27:49:0E:C0` |
| 3     | Requête ARP | x         | `router` `08:00:27:E8:42:D8` | x              | Broadcast `FF:FF:FF:FF:FF:FF` |
| 4     | Réponse ARP | x         | `marcel` `08:00:27:A3:FF:28` | x              | `router`  `08:00:27:E8:42:D8` |
| 5     | Ping        | 10.2.1.11 | `node1`  `08:00:27:49:0E:C0` | 10.2.2.12      | `router`  `08:00:27:E8:42:D8` |
| 6     | Ping        | 10.2.1.11 | `router` `08:00:27:E8:42:D8` | 10.2.2.12      | `marcel`  `08:00:27:A3:FF:28` |
| 7     | Pong        | 10.2.2.12 | `marcel` `08:00:27:A3:FF:28` | 10.2.1.11      | `router`  `08:00:27:E8:42:D8` |
| 8     | Pong        | 10.2.2.12 | `router` `08:00:27:E8:42:D8` | 10.2.1.11      | `node1`   `08:00:27:49:0E:C0` |

### 3. Accès internet

**🌞 Donnez un accès internet à vos machines**

```
[clem@node1 ~]$ ip r s
default via 10.2.1.254 dev enp0s8 proto static metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100
10.2.2.0/24 via 10.2.1.254 dev enp0s8 proto static metric 100
[clem@node1 ~]$ ping 92.243.16.143
PING 92.243.16.143 (92.243.16.143) 56(84) bytes of data.
64 bytes from 92.243.16.143: icmp_seq=1 ttl=52 time=17.7 ms
64 bytes from 92.243.16.143: icmp_seq=2 ttl=52 time=16.1 ms
^C
--- 92.243.16.143 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 16.134/16.906/17.679/0.783 ms
```

```
[clem@marcel ~]$ ip r s
default via 10.2.2.254 dev enp0s8
10.2.1.0/24 via 10.2.2.254 dev enp0s8 proto static metric 100
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
[clem@marcel ~]$ ping 92.243.16.143
PING 92.243.16.143 (92.243.16.143) 56(84) bytes of data.
64 bytes from 92.243.16.143: icmp_seq=1 ttl=52 time=15.6 ms
64 bytes from 92.243.16.143: icmp_seq=2 ttl=52 time=17.5 ms
^C
--- 92.243.16.143 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 15.610/16.542/17.474/0.932 ms
```

```
[clem@node1 ~]$ [clem@node1 ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 1.1.1.1
[clem@node1 ~]$ dig ynov.com

...
;; ANSWER SECTION:
ynov.com.               6107    IN      A       92.243.16.143

;; Query time: 17 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sat Sep 25 18:32:42 CEST 2021
;; MSG SIZE  rcvd: 53

[clem@node1 ~]$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=52 time=15.4 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=52 time=16.7 ms
^C
--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1080ms
rtt min/avg/max/mdev = 15.432/16.068/16.705/0.649 ms
```

```
[clem@marcel ~]$ [clem@marcel ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 1.1.1.1
[clem@marcel ~]$ dig ynov.com

...
;; ANSWER SECTION:
ynov.com.               246     IN      A       92.243.16.143

;; Query time: 21 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sat Sep 25 18:33:53 CEST 2021
;; MSG SIZE  rcvd: 53

[clem@marcel ~]$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=52 time=15.2 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=52 time=16.4 ms
^C
--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 15.185/15.798/16.412/0.626 ms
```

**🌞 Analyse de trames**

![](Reseaux/TP2/Images/ping_routage_internet.PNG)

| ordre | type trame | IP source           | MAC source                   | IP destination           | MAC destination              |
|-------|------------|---------------------|------------------------------|--------------------------|------------------------------|
| 1     | Ping       | `node1` `10.2.1.12` | `node1`  `08:00:27:49:0E:C0` | `8.8.8.8`                | `router` `08:00:27:B7:DD:3F` |
| 2     | Ping       | `node1` `10.2.1.12` | `router` `08:00:27:B7:DD:3F` | `8.8.8.8`                |  x                           |
| 3     | Pong       | `8.8.8.8`           |  x                           | `node1` `10.2.1.12`      | `router` `08:00:27:B7:DD:3F` |
| 4     | Pong       | `8.8.8.8`           |  x                           | `node1` `10.2.1.12`      | `node1`  `08:00:27:49:0E:C0` |

## III. DHCP

### 1. Mise en place du serveur DHCP

**🌞 Sur la machine node1.net1.tp2, vous installerez et configurerez un serveur DHCP**

Installation du serveur : 

```
[clem@node1 ~]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
authoritative;
subnet 10.2.1.0 netmask 255.255.255.0 {
  range 10.2.1.2 10.2.1.252;

}
[clem@node1 ~]$ systemctl enable --now dhcpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
...
==== AUTHENTICATION COMPLETE ====
[clem@node1 ~]$ sudo firewall-cmd --add-service=dhcp
[sudo] password for clem:
success
[clem@node1 ~]$ sudo firewall-cmd --runtime-to-permanent
success
```

Récupération IP : 

`nmcli connection modify enp0s8 ipv4.method auto`
`nmcli connection down enp0s8`
`nmcli connection up enp0s8`

```
[clem@node2 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
BOOTPROTO=dhcp
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
...

[clem@node2 ~]$ ip a
...
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:98:47:69 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.2/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 740sec preferred_lft 740sec
    inet6 fe80::a00:27ff:fe98:4769/64 scope link
       valid_lft forever preferred_lft forever
```

**🌞 Améliorer la configuration du DHCP**

```
[clem@node1 ~]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
authoritative;
subnet 10.2.1.0 netmask 255.255.255.0 {
  range 10.2.1.2 10.2.1.252;
  # route par défaut
  option routers 10.2.1.254;
  option subnet-mask 255.255.255.0;
  # serveur DNS
  option domain-name-servers 1.1.1.1;

}
```

node2.net1.tp2 doit avoir une IP : 

```
[clem@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:98:47:69 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.2/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 944sec preferred_lft 944sec
    inet6 fe80::a00:27ff:fe98:4769/64 scope link
       valid_lft forever preferred_lft forever
```

```
[clem@node2 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=2.84 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.499 ms
^C
--- 10.2.1.254 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.499/1.670/2.842/1.172 ms
```

Il doit avoir une route par défaut.

```
[clem@node2 ~]$ ip r s
default via 10.2.1.254 dev enp0s8 proto dhcp metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.2 metric 100
```

```
[clem@node2 ~]$ ping 92.243.16.143
PING 92.243.16.143 (92.243.16.143) 56(84) bytes of data.
64 bytes from 92.243.16.143: icmp_seq=1 ttl=52 time=23.2 ms
64 bytes from 92.243.16.143: icmp_seq=2 ttl=52 time=13.8 ms
^C
--- 92.243.16.143 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 13.767/18.483/23.200/4.718 ms
```

Il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms.

```
[clem@node2 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 25806
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10800   IN      A       92.243.16.143

;; Query time: 18 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sat Sep 25 20:31:52 CEST 2021
;; MSG SIZE  rcvd: 53
```

```
[clem@node2 ~]$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=52 time=14.5 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=52 time=16.1 ms
^C
--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 14.524/15.319/16.115/0.805 ms
```

### 2. Analyse de trames

- videz les tables ARP des machines concernées
    `sudo ip neigh flush all`
    
- répéter une opération de renouvellement de bail DHCP, et demander une nouvelle IP afin de générer un échange DHCP
    `sudo dhclient -r`
    `sudo dhclient enp0s8`
    
![](Reseaux/TP2/Images/echange_dhcp.PNG)

| ordre | type trame    | IP source | MAC source                   | IP destination | MAC destination               |
|-------|---------------|-----------|------------------------------|----------------|-------------------------------|
| 1     | ARP Request   | x         | `node2`  `08:00:27:98:47:49` | x              | Broadcast `FF:FF:FF:FF:FF:FF` |
| 2     | ARP Reply     | 10.2.1.11 | `node1`  `08:00:27:49:0E:C0` | x              | `node2`   `08:00:27:98:47:49` |
| 3     | DHCP Release  | 10.2.1.2  | `node2`  `08:00:27:98:47:49` | 10.2.1.11      | `node1`   `08:00:27:49:0E:C0` |
| 4     | DHCP Discover | 0.0.0.0   |  x                           | 255.255.255.255| Broadcast `FF:FF:FF:FF:FF:FF` |
| 5     | Ping          | 10.2.1.11 | `node1`  `08:00:27:49:0E:C0` | 10.2.1.2       | x                             |
| 6     | DHCP Offer    | 10.2.1.11 | `node1`  `08:00:27:49:0E:C0` | 10.2.1.2       | x                             |
| 7     | DHCP Request  | 0.0.0.0   |  x                           | 255.255.255.255| Broadcast `FF:FF:FF:FF:FF:FF` |
| 8     | DHCP ACK      | 10.2.1.11 | `node1`  `08:00:27:49:0E:C0` | 10.2.1.2       | `node2`   `08:00:27:98:47:49` |
| 9     | ARP Request   | 0.0.0.0   | `node2`  `08:00:27:98:47:49` | x              | Broadcast `FF:FF:FF:FF:FF:FF` |
| 9     | ARP Request   | 0.0.0.0   | `node2`  `08:00:27:98:47:49` | x              | Broadcast `FF:FF:FF:FF:FF:FF` |
