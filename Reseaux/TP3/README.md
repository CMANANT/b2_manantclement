# TP3 : Progressons vers le réseau d'infrastructure

## I. (mini)Architecture réseau

### 1. Adressage

**🌞 Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme :**

| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
|---------------|-------------------|-------------------|-----------------------------|--------------------|-------------------|
| `server1`     | `10.3.0.0`        | `255.255.255.128` | 126                         | `10.3.0.126`       | `10.3.0.127`      |
| `client1`     | `10.3.0.128`      | `255.255.255.192` | 62                          | `10.3.0.190`       | `10.3.0.191`      |
| `server2`     | `10.3.0.192`      | `255.255.255.240` | 14                          | `10.3.0.206`       | `10.3.0.207`      |

**🌞 Vous remplirez aussi au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, le 🗃️ tableau d'adressage 🗃️ suivant :**

| Nom machine          | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|----------------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3`         | `10.3.0.190/26`      | `10.3.0.126/25`      | `10.3.0.206/28`      | Carte NAT             |
| `dhcp.client1.tp3`   | `10.3.0.130/26`      |  x                   |  x                   | `10.3.0.190/26`       |
| `marcel.client1.tp3` | `10.3.0.131/26`      |  x                   |  x                   | `10.3.0.190/26`       |
| `dns.server1.tp3`    |  x                   | `10.3.0.2/25`        |  x                   | `10.3.0.126/25`       |
| `johnny.client1.tp3` | `10.3.0.132/26`      |  x                   |  x                   | `10.3.0.190/26`       |
| `web1.server2.tp3`   |  x                   |  x                   |  `10.3.0.194/28`     | `10.3.0.206/28`       |
| `nfs1.server2.tp3`   |  x                   |  x                   |  `10.3.0.195/28`     | `10.3.0.206/28`       |

### 2. Routeur

**🌞 Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :**

- il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle

```
[clem@router ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b2:91:36 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86182sec preferred_lft 86182sec
    inet6 fe80::a00:27ff:feb2:9136/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:61:45:0a brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.126/25 brd 10.3.0.127 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe61:450a/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:29:88:2c brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.190/26 brd 10.3.0.191 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe29:882c/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:2c:b7:95 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.206/28 brd 10.3.0.207 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe2c:b795/64 scope link
       valid_lft forever preferred_lft forever
```
       
- il a un accès internet

```
[clem@router ~]$ ping -c 2 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=19.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=20.9 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 19.906/20.402/20.898/0.496 ms
```

- il a de la résolution de noms

```
[clem@router ~]$ dig ynov.com
[...]
;; ANSWER SECTION:
ynov.com.               5603    IN      A       92.243.16.143

;; Query time: 18 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 30 16:04:12 CEST 2021
;; MSG SIZE  rcvd: 53
```

- il porte le nom router.tp3*

```
[clem@router ~]$ hostname
router.tp3
```

- n'oubliez pas d'activer le routage sur la machine

```
[clem@router ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s10 enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## II. Services d'infra

### 1. Serveur DHCP

**🌞 Mettre en place une machine qui fera office de serveur DHCP dans le réseau client1. Elle devra :**

- porter le nom dhcp.client1.tp3

```
[clem@dhcp ~]$ hostname
dhcp.client1.tp3
```

- donner une IP aux machines clients qui le demande
- leur donner l'adresse de leur passerelle
- leur donner l'adresse d'un DNS utilisable

# 📁 Fichier dhcpd.conf a dl

```
[clem@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
# default lease time

default-lease-time 600;
# max lease time

max-lease-time 7200;
# this DHCP server to be declared valid

authoritative;

# specify network address and subnetmask
subnet 10.3.0.128 netmask 255.255.255.192 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.0.131 10.3.0.189;
    # specify gateway
    option routers 10.3.0.190;
    # specify DNS
    option domain-name-servers 1.1.1.1;
}
```

**🌞 Mettre en place un client dans le réseau client1**

- de son p'tit nom marcel.client1.tp3

```
[clem@marcel ~]$ hostname
marcel.client1.tp3
```

- la machine récupérera une IP dynamiquement grâce au serveur DHCP

```
[clem@marcel ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
BOOTPROTO=dhcp
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
[clem@marcel ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:20:a1:91 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.131/26 brd 10.3.0.191 scope global dynamic noprefixroute enp0s8
       valid_lft 363sec preferred_lft 363sec
    inet6 fe80::a00:27ff:fe20:a191/64 scope link
       valid_lft forever preferred_lft forever
```

- ainsi que sa passerelle et une adresse d'un DNS utilisable

```
[clem@marcel ~]$ ip r s
default via 10.3.0.190 dev enp0s8 proto dhcp metric 100
10.3.0.128/26 dev enp0s8 proto kernel scope link src 10.3.0.131 metric 100
[clem@marcel ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
search client1.tp3
nameserver 1.1.1.1
```

**🌞 Depuis marcel.client1.tp3**

- prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP

```
[clem@marcel ~]$ ping -c 2 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=19.10 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=21.2 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 19.975/20.566/21.158/0.608 ms
[clem@marcel ~]$ dig ynov.com
[...]
;; ANSWER SECTION:
ynov.com.               2908    IN      A       92.243.16.143

;; Query time: 20 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 30 16:42:02 CEST 2021
;; MSG SIZE  rcvd: 53
```

- à l'aide de la commande traceroute, prouver que marcel.client1.tp3 passe par router.tp3 pour sortir de son réseau

```
[clem@marcel ~]$ traceroute ynov.com
traceroute to ynov.com (92.243.16.143), 30 hops max, 60 byte packets
 1  _gateway (10.3.0.190)  0.736 ms  0.692 ms  0.676 ms
 2  10.0.2.2 (10.0.2.2)  0.662 ms  0.647 ms  0.585 ms
 3  10.0.2.2 (10.0.2.2)  4.055 ms  4.243 ms  4.892 ms
```

`10.3.0.190` est bien l'IP de router.tp3.

### 2. Serveur DNS

#### A. Our own DNS server

#### B. SETUP copain

**🌞 Mettre en place une machine qui fera office de serveur DNS**

- dans le réseau server1
- de son p'tit nom dns1.server1.tp3
```
[clem@dns1 ~]$ hostname
dns1.server1.tp3
```
- 📝checklist📝
- il faudra lui ajouter un serveur DNS public connu, afin qu'il soit capable de résoudre des noms publics comme google.com
    - conf classique avec le fichier /etc/resolv.conf ou les fichiers de conf d'interface
```
[clem@dns1 ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
search server1.tp3
nameserver 1.1.1.1
```
- comme pour le DHCP, on part sur "rocky linux dns server" on Google pour les détails de l'install du serveur DNS
    - le paquet que vous allez installer devrait s'appeler bind : c'est le nom du serveur DNS le plus utilisé au monde
```
[clem@dns1 ~]$ sudo dnf -y install bind bind-utils
[...]
Complete!
```
- il y aura plusieurs fichiers de conf :
    - un fichier de conf principal named.conf
    - des fichiers de zone "forward"
        - permet d'indiquer une correspondance nom -> IP
        - un fichier par zone forward
    - vous ne mettrez pas en place de zones reverse, uniquement les forward
    - on ne met PAS les clients dans les fichiers de zone car leurs adresses IP peuvent changer (ils les récupèrent à l'aide du DHCP)
        - donc votre DNS gérera deux zones : server1.tp3 et server2.tp3
        - les réseaux où les IPs sont définies de façon statique !

named.conf : 
```
[clem@dns1 ~]$ sudo cat /etc/named.conf
[sudo] password for clem:
//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

options {
        listen-on port 53 { any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { any; };

        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;

        dnssec-enable yes;
        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
        type hint;
        file "named.ca";
};

zone "server1.tp3" IN {
        type master;
        file "/var/named/server1.tp3.forward";
        allow-update { none; };
};

zone "server2.tp3" IN {
        type master;
        file "/var/named/server2.tp3.forward";
        allow-update { none; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

server1.tp3.forward : 

```
[clem@dns1 ~]$ sudo cat /var/named/server1.tp3.forward
$TTL 86400
@   IN  SOA     dns1.server1.tp3. root.server1.tp3. (
         2021100404  ;Serial
         3600        ;Refresh
         1800        ;Retry
         604800      ;Expire
         86400       ;Minimum TTL
)
        ; Set your Name Servers here
@         IN  NS      dns1.server1.tp3.
         ; define Name Server's IP address
@         IN  A       10.3.0.2

; Set each IP address of a hostname. Sample A records.
dns1           IN  A       10.3.0.2
```

server2.tp3.forward : 

```
[clem@dns1 ~]$ sudo cat /var/named/server2.tp3.forward
$TTL 86400
@   IN  SOA     dns1.server2.tp3. root.server2.tp3. (
         2021100404  ;Serial
         3600        ;Refresh
         1800        ;Retry
         604800      ;Expire
         86400       ;Minimum TTL
)
        ; Set your Name Servers here
@         IN  NS      dns1.server2.tp3.
         ; define Name Server's IP address
@         IN  A       10.3.0.2

; Set each IP address of a hostname. Sample A records.
dns1           IN  A       10.3.0.2
```

**🌞 Tester le DNS depuis marcel.client1.tp3**

- définissez manuellement l'utilisation de votre serveur DNS
```
[clem@marcel ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 10.3.0.2
```
- essayez une résolution de nom avec dig
    - une résolution de nom classique
        - dig NOM pour obtenir l'IP associée à un nom
        - on teste la zone forward
```
[clem@marcel ~]$ dig dns1.server1.tp3
[...]
;; ANSWER SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.0.2

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; Query time: 1 msec
;; SERVER: 10.3.0.2#53(10.3.0.2)
;; WHEN: Mon Oct 04 16:38:17 CEST 2021
;; MSG SIZE  rcvd: 103
```
- prouvez que c'est bien votre serveur DNS qui répond pour chaque dig
`;; SERVER: 10.3.0.2#53(10.3.0.2)`

**🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds**

- les serveurs, on le fait à la main
router.tp3 : 
```
[clem@router ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 10.3.0.2
```

dhcp.client1.tp3 : 
```
[clem@dhcp ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
search client1.tp3
nameserver 10.3.0.2
```

- les clients, c'est fait via DHCP
dhcp.client1.tp3
```
[clem@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
# default lease time

default-lease-time 600;
# max lease time

max-lease-time 7200;
# this DHCP server to be declared valid

authoritative;

# specify network address and subnetmask
subnet 10.3.0.128 netmask 255.255.255.192 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.0.131 10.3.0.189;
    # specify gateway
    option routers 10.3.0.190;
    # specify DNS
    option domain-name-servers 10.3.0.2;
}
```

### 3. Get deeper

#### A. DNS forwarder

**🌞 Affiner la configuration du DNS**

- faites en sorte que votre DNS soit désormais aussi un forwarder DNS
- c'est à dire que s'il ne connaît pas un nom, il ira poser la question à quelqu'un d'autre

```
[clem@dns1 ~]$ sudo cat /etc/named.conf
[...]
        recursion yes;
[...]
```

**🌞 Test !**

- vérifier depuis marcel.client1.tp3 que vous pouvez résoudre des noms publics comme google.com en utilisant votre propre serveur DNS (commande dig)
```
[clem@marcel ~]$ dig google.com
[...]
;; ANSWER SECTION:
google.com.             300     IN      A       216.58.206.238
[...]
;; Query time: 94 msec
;; SERVER: 10.3.0.2#53(10.3.0.2)
;; WHEN: Mon Oct 04 16:48:26 CEST 2021
;; MSG SIZE  rcvd: 331
```
- pour que ça fonctionne, il faut que dns1.server1.tp3 soit lui-même capable de résoudre des noms, avec 1.1.1.1 par exemple
```
[clem@dns1 ~]$ dig ynov.com @1.1.1.1
[...]
;; ANSWER SECTION:
ynov.com.               2484    IN      A       92.243.16.143

;; Query time: 16 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Mon Oct 04 16:49:52 CEST 2021
;; MSG SIZE  rcvd: 53
```

#### B. On revient sur la conf du DHCP

**🌞 Affiner la configuration du DHCP**

- faites en sorte que votre DHCP donne désormais l'adresse de votre serveur DNS aux clients
```
[clem@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
# default lease time

default-lease-time 600;
# max lease time

max-lease-time 7200;
# this DHCP server to be declared valid

authoritative;

# specify network address and subnetmask
subnet 10.3.0.128 netmask 255.255.255.192 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.0.131 10.3.0.189;
    # specify gateway
    option routers 10.3.0.190;
    # specify DNS
    option domain-name-servers 10.3.0.2;
}
```
- créer un nouveau client johnny.client1.tp3 qui récupère son IP, et toutes les nouvelles infos, en DHCP

```
[clem@johnny ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
[sudo] password for clem:
TYPE=Ethernet
BOOTPROTO=dhcp
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
[clem@johnny ~]$ ip a
[...]
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:36:5c:67 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.132/26 brd 10.3.0.191 scope global dynamic noprefixroute enp0s8
       valid_lft 403sec preferred_lft 403sec
    inet6 fe80::a00:27ff:fe36:5c67/64 scope link
       valid_lft forever preferred_lft forever
[clem@johnny ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
search client1.tp3
nameserver 10.3.0.2
```

## III. Services métier

### 1. Serveur Web

**🌞 Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients**

- réseau server2
- hello web1.server2.tp3 !
- 📝checklist📝
- vous utiliserez le serveur web que vous voudrez, le but c'est d'avoir un serveur web fast, pas d'y passer 1000 ans :)
    - réutilisez votre serveur Web du TP1 Linux
    - ou montez un bête NGINX avec la page d'accueil (ça se limite à un dnf install puis systemctl start nginx)
    - ou ce que vous voulez, du moment que c'est fast
    - dans tous les cas, n'oubliez pas d'ouvrir le port associé dans le firewall pour que le serveur web soit joignable

```
[clem@web1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disa>
   Active: active (running) since Tue 2021-10-05 16:27:20 CEST; 6min ago
```

**🌞 Test test test et re-test**

- testez que votre serveur web est accessible depuis marcel.client1.tp3
    - utilisez la commande curl pour effectuer des requêtes HTTP

```
[clem@marcel ~]$ curl 10.3.0.194
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
[...]
</html>
```

### 2. Partage de fichiers

#### A. L'introduction wola

#### B. Le setup wola

**🌞 Setup d'une nouvelle machine, qui sera un serveur NFS**

- réseau server2
- son nooooom : nfs1.server2.tp3 !
- 📝checklist📝
- je crois que vous commencez à connaître la chanson... Google "nfs server rocky linux"
    - ce lien me semble être particulièrement simple et concis
- vous partagerez un dossier créé à cet effet : /srv/nfs_share/

```
[clem@nfs1 ~]$ sudo cat /etc/idmapd.conf
[sudo] password for clem:
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = server2.tp3
[...]

[clem@nfs1 ~]$ sudo cat /etc/exports
/srv/nfs_share/ 10.3.0.192/28(rw,no_root_squash)

[clem@nfs1 ~]$ ls -al /srv/
total 0
drwxr-xr-x.  3 root root  23 Oct  5 17:16 .
dr-xr-xr-x. 17 root root 224 Sep 15 15:25 ..
drwxr-xr-x.  2 root root   6 Oct  5 17:16 nfs_share

[clem@nfs1 ~]$ sudo systemctl status nfs-server
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; vendor preset: disabled)
  Drop-In: /run/systemd/generator/nfs-server.service.d
           └─order-with-mounts.conf
   Active: active (exited) since Mon 2021-10-18 21:36:56 CEST; 2min 49s ago
  Process: 1151 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gssproxy ; >
  Process: 1077 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
  Process: 1071 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUCCESS)
 Main PID: 1151 (code=exited, status=0/SUCCESS)
    Tasks: 0 (limit: 4946)
   Memory: 0B
   CGroup: /system.slice/nfs-server.service

Oct 18 21:36:55 nfs1.server2.tp3 systemd[1]: Starting NFS server and services...
Oct 18 21:36:56 nfs1.server2.tp3 systemd[1]: Started NFS server and services.

[1]+  Stopped                 sudo systemctl status nfs-server
```

**🌞 Configuration du client NFS**

- effectuez de la configuration sur web1.server2.tp3 pour qu'il accède au partage NFS
- le partage NFS devra être monté dans /srv/nfs/
- sur le même site, y'a ça

```
[clem@web1 ~]$ sudo cat /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = server2.tp3
[...]

[clem@web1 ~]$ sudo mount -t nfs nfs1.server2.tp3:/srv/nfs_share /srv/nfs/

[clem@web1 ~]$ df -hT
Filesystem                      Type      Size  Used Avail Use% Mounted on
devtmpfs                        devtmpfs  387M     0  387M   0% /dev
tmpfs                           tmpfs     405M     0  405M   0% /dev/shm
tmpfs                           tmpfs     405M  5.6M  400M   2% /run
tmpfs                           tmpfs     405M     0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root             xfs       6.2G  2.2G  4.1G  34% /
/dev/sda1                       xfs      1014M  241M  774M  24% /boot
tmpfs                           tmpfs      81M     0   81M   0% /run/user/1000
nfs1.server2.tp3:/srv/nfs_share nfs4      6.2G  2.1G  4.2G  34% /srv/nfs

[clem@web1 ~]$ sudo cat /etc/fstab
[...]
nfs1.server2.tp3:/srv/nfs_share /srv/nfs        nfs     defaults        0 0
```

**🌞 TEEEEST**

- tester que vous pouvez lire et écrire dans le dossier /srv/nfs depuis web1.server2.tp3
```
[clem@web1 ~]$ touch /srv/nfs/toto
[clem@web1 ~]$ vim /srv/nfs/toto
[clem@web1 ~]$ cat /srv/nfs/toto
TOTO
```
- vous devriez voir les modifications du côté de  nfs1.server2.tp3 dans le dossier /srv/nfs_share/
```
[clem@nfs1 ~]$ cat /srv/nfs_share/toto
TOTO
```

## IV. Un peu de théorie : TCP et UDP

**🌞 Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP :**

TCP : SSH, HTTP, NFS
UDP : DNS

**📁 Captures réseau tp3_ssh.pcap, tp3_http.pcap, tp3_dns.pcap et tp3_nfs.pcap**

- *[tp3_ssh.pcap](./Annexes/tp3_ssh.pcap)*
- *[tp3_http.pcap](./Annexes/tp3_http.pcap)*
- *[tp3_dns.pcap](./Annexes/tp3_dns.pcap)*
- *[tp3_nfs.pcap](./Annexes/tp3_nfs.pcap)*

**🌞 Capturez et mettez en évidence un 3-way handshake**

**📁 Capture réseau tp3_3way.pcap**

- *[tp3_3way.pcap](./Annexes/tp3_3way.pcap)*

Les trois premières lignes de ce fichier mettent en évidence un 3-way handshake.

## V. El final

**🌞 Bah j'veux un schéma.**

- *[Schéma](./Annexes/Schema.png)*

**🗃️ tableau des réseaux 🗃️**

| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
|---------------|-------------------|-------------------|-----------------------------|--------------------|-------------------|
| `server1`     | `10.3.0.0`        | `255.255.255.128` | 126                         | `10.3.0.126`       | `10.3.0.127`      |
| `client1`     | `10.3.0.128`      | `255.255.255.192` | 62                          | `10.3.0.190`       | `10.3.0.191`      |
| `server2`     | `10.3.0.192`      | `255.255.255.240` | 14                          | `10.3.0.206`       | `10.3.0.207`      |

**🗃️ tableau d'adressage 🗃️**

| Nom machine          | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|----------------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3`         | `10.3.0.190/26`      | `10.3.0.126/25`      | `10.3.0.206/28`      | Carte NAT             |
| `dhcp.client1.tp3`   | `10.3.0.130/26`      |  x                   |  x                   | `10.3.0.190/26`       |
| `marcel.client1.tp3` | `10.3.0.131/26`      |  x                   |  x                   | `10.3.0.190/26`       |
| `dns.server1.tp3`    |  x                   | `10.3.0.2/25`        |  x                   | `10.3.0.126/25`       |
| `johnny.client1.tp3` | `10.3.0.132/26`      |  x                   |  x                   | `10.3.0.190/26`       |
| `web1.server2.tp3`   |  x                   |  x                   |  `10.3.0.194/28`     | `10.3.0.206/28`       |
| `nfs1.server2.tp3`   |  x                   |  x                   |  `10.3.0.195/28`     | `10.3.0.206/28`       |

**🌞 Et j'veux des fichiers aussi, tous les fichiers de conf du DNS**

**📁 Fichiers de zone**

- *[server1.tp3.forward](./Annexes/server1.tp3.forward)*
- *[server2.tp3.forward](./Annexes/server2.tp3.forward)*

**📁 Fichier de conf principal DNS named.conf**

- *[named.conf](./Annexes/named.conf)*
