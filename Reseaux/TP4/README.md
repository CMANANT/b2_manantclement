# TP4 : Vers un réseau d'entreprise

## I. Dumb switch

### 1. Topologie 1

### 2. Adressage topologie 1

### 3. Setup topologie 1

**🌞 Commençons simple**

- définissez les IPs statiques sur les deux VPCS
    ```
    PC1> ip 10.1.1.1/24
    Checking for duplicate address...
    PC1 : 10.1.1.1 255.255.255.0
    ```
    ```
    PC2> ip 10.1.1.2/24
    Checking for duplicate address...
    PC2 : 10.1.1.2 255.255.255.0
    ```
- ping un VPCS depuis l'autre
    ```
    PC1> ping 10.1.1.2

    84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=6.333 ms
    84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=4.676 ms
    ```
    ```
    PC2> ping 10.1.1.1

    84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=6.656 ms
    84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=10.328 ms
    ```
    
## II. VLAN

### 1. Topologie 2

### 2. Adressage topologie 2

### 3. Setup topologie 2

**🌞 Adressage**

- définissez les IPs statiques sur tous les VPCS
    ```
    PC3> ip 10.1.1.3/24
    Checking for duplicate address...
    PC3 : 10.1.1.3 255.255.255.0
    ```
- vérifiez avec des ping que tout le monde se ping
    ```
    PC1> ping 10.1.1.3

    84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=7.633 ms
    84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=6.101 ms
    ```

    ```
    PC2> ping 10.1.1.3

    84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=6.613 ms
    84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=11.179 ms
    ```

    ```
    PC3> ping 10.1.1.1

    84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=10.219 ms
    84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=8.308 ms
    ^C
    PC3> ping 10.1.1.2

    84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=10.828 ms
    84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=11.512 ms
    ```

**🌞 Configuration des VLANs**

- référez-vous à la section VLAN du mémo Cisco
- déclaration des VLANs sur le switch sw1
    ```
    Switch>enable
    Switch#conf t
    Enter configuration commands, one per line.  End with CNTL/Z.
    Switch(config)#vlan 10
    Switch(config-vlan)#name 10
    Switch(config-vlan)#exit
    Switch(config)#vlan 20
    Switch(config-vlan)#name 20
    Switch(config-vlan)#exit
    Switch(config)#exit
    ```
- ajout des ports du switches dans le bon VLAN (voir le tableau d'adressage de la topo 2 juste au dessus)
    - ici, tous les ports sont en mode access : ils pointent vers des clients du réseau
        ```
        Switch#sh vlan br

        VLAN Name                             Status    Ports
        ---- -------------------------------- --------- -------------------------------
        1    default                          active    Gi0/1, Gi1/0, Gi1/1, Gi1/2
                                                        Gi1/3, Gi2/0, Gi2/1, Gi2/2
                                                        Gi2/3, Gi3/0, Gi3/1, Gi3/2
                                                        Gi3/3
        10   10                               active    Gi0/0, Gi0/2
        20   20                               active    Gi0/3
        [...]
        ```

**🌞 Vérif**

- pc1 et pc2 doivent toujours pouvoir se ping
    ```
    PC1> ping 10.1.1.2

    84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=9.882 ms
    84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=2.153 ms
    ```

    ```
    PC2> ping 10.1.1.1

    84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=11.337 ms
    84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=5.787 ms
    ```
- pc3 ne ping plus personne

    ```
    PC3> ping 10.1.1.1

    host (10.1.1.1) not reachable

    PC3> ping 10.1.1.2

    host (10.1.1.2) not reachable
    ```
    
## III. Routing

### 1. Topologie 3

### 2. Adressage topologie 3

### 3. Setup topologie 3

**🌞 Adressage**

- définissez les IPs statiques sur toutes les machines sauf le routeur
    ```
    PC1> sh ip a

    NAME   IP/MASK              GATEWAY           MAC                DNS
    PC1    10.1.1.1/24          0.0.0.0           00:50:79:66:68:00
    ```
    ```
    PC2> sh ip a

    NAME   IP/MASK              GATEWAY           MAC                DNS
    PC2    10.1.1.2/24          0.0.0.0           00:50:79:66:68:01
    ```
    ```
    PC3> sh ip a

    NAME   IP/MASK              GATEWAY           MAC                DNS
    PC3    10.2.2.1/24          0.0.0.0           00:50:79:66:68:02
    ```
    ```
    [clem@web1 ~]$ ip a
    [...]
        inet 10.3.3.1/24 brd 10.3.3.255 scope global noprefixroute enp0s3
        [...]
    ```
    
**🌞 Configuration des VLANs**

- référez-vous au mémo Cisco
- déclaration des VLANs sur le switch sw1
- ajout des ports du switches dans le bon VLAN (voir le tableau d'adressage de la topo 2 juste au dessus)
    ```
    Switch#show vlan

    VLAN Name                             Status    Ports
    ---- -------------------------------- --------- -------------------------------
    1    default                          active    Gi1/1, Gi1/2, Gi1/3, Gi2/0
                                                    Gi2/1, Gi2/2, Gi2/3, Gi3/0
                                                    Gi3/1, Gi3/2, Gi3/3
    10   10                               active
    11   clients                          active    Gi0/0, Gi0/2
    12   admins                           active    Gi0/3
    13   servers                          active    Gi1/0
    ```
- il faudra ajouter le port qui pointe vers le routeur comme un trunk : c'est un port entre deux équipements réseau (un switch et un routeur)

    ```
    Switch#show interface trunk

    Port        Mode             Encapsulation  Status        Native vlan
    Gi0/1       on               802.1q         trunking      1

    Port        Vlans allowed on trunk
    Gi0/1       1-4094

    Port        Vlans allowed and active in management domain
    Gi0/1       1,10-13,20

    Port        Vlans in spanning tree forwarding state and not pruned
    Gi0/1       1,10-13,20
    ```
    
**➜ Pour le routeur**

- référez-vous au mémo Cisco
- ici, on va avoir besoin d'un truc très courant pour un routeur : qu'il porte plusieurs IP sur une unique interface
    - avec Cisco, on crée des "sous-interfaces" sur une interface
    - et on attribue une IP à chacune de ces sous-interfaces
- en plus de ça, il faudra l'informer que, pour chaque interface, elle doit être dans un VLAN spécifique

**🌞 Config du routeur**

- attribuez ses IPs au routeur
    - 3 sous-interfaces, chacune avec son IP et un VLAN associé
    ```
    R1(config)#interface fastEthernet 0/0.
    R1(config-subif)#encapsulation dot1Q 11
    R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
    R1(config-subif)#exit
    
    R1(config)#interface fastEthernet 0/0.12
    R1(config-subif)#encapsulation dot1Q 12
    R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
    R1(config-subif)#exit
    
    R1(config)#interface fastEthernet 0/0.13
    R1(config-subif)#encapsulation dot1Q 13
    R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
    R1(config-subif)#exit
    
    R1#conf t
    Enter configuration commands, one per line.  End with CNTL/Z.
    R1(config)#interface fastEthernet 0/0
    R1(config-if)#no shut
    R1(config-if)#exit
    R1(config)#
    *Mar  1 01:07:26.663: %LINK-3-UPDOWN: Interface FastEthernet0/0, changed state to up
    *Mar  1 01:07:27.663: %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0, changed state to up
    R1(config)#exit
    *Mar  1 01:07:32.503: %SYS-5-CONFIG_I: Configured from console by console
    R1#show ip int br
    Interface                  IP-Address      OK? Method Status                Protocol
    FastEthernet0/0            unassigned      YES unset  up                    up
    FastEthernet0/0.10         192.168.1.254   YES manual up                    up
    FastEthernet0/0.11         10.1.1.254      YES manual up                    up
    FastEthernet0/0.12         10.2.2.254      YES manual up                    up
    FastEthernet0/0.13         10.3.3.254      YES manual up                    up
    FastEthernet0/0.20         192.168.2.254   YES manual up                    up
    ```


**🌞 Vérif**

- tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau
    ```
    PC1> ping 10.1.1.254

    84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=20.312 ms
    84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=16.505 ms
    ```
    ```
    PC2> ping 10.1.1.254

    84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=40.928 ms
    84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=21.843 ms
    ```
    ```
    PC3> ping 10.2.2.254

    84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=10.332 ms
    84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=16.480 ms
    ```
    ```
    [clem@web1 ~]$ ping -c 2 10.3.3.254
    PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
    64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=25.3 ms
    64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=37.9 ms

    --- 10.3.3.254 ping statistics ---
    2 packets transmitted, 2 received, 0% packet loss, time 1002ms
    rtt min/avg/max/mdev = 25.335/31.594/37.853/6.259 ms
    ```
- en ajoutant une route vers les réseaux, ils peuvent se ping entre eux
    - ajoutez une route par défaut sur les VPCS
        ```
        PC1> ip 10.1.1.1/24 10.1.1.254
        Checking for duplicate address...
        PC1 : 10.1.1.1 255.255.255.0 gateway 10.1.1.254
        ```
        ```
        PC2> ip 10.1.1.2/24 10.1.1.254
        Checking for duplicate address...
        PC2 : 10.1.1.2 255.255.255.0 gateway 10.1.1.254
        ```
        ```
        PC3> ip 10.2.2.1/24 10.2.2.254
        Checking for duplicate address...
        PC3 : 10.2.2.1 255.255.255.0 gateway 10.2.2.254
        ```
    - ajoutez une route par défaut sur la machine virtuelle
        ```
        [clem@web1 ~]$ sudo ip route add default via 10.3.3.254 dev 
        [clem@web1 ~]$ sudo cat /etc/sysconfig/network
        # Created by anaconda
        GATEWAY=10.3.3.254
        ```
    - testez des ping entre les réseaux
        ```
        PC1> ping 10.2.2.1

        84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=39.242 ms
        84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=19.995 ms
        ```
        ```
        PC2> ping 10.3.3.1

        84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=17.934 ms
        84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=25.082 ms
        ```
        ```
        PC3> ping 10.3.3.1

        84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=23.306 ms
        84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=26.422 ms
        ```
        
## IV. NAT

### 1. Topologie 4

### 2. Adressage topologie 4

### 3. Setup topologie 4

**🌞 Ajoutez le noeud Cloud à la topo**

- branchez à eth1 côté Cloud
- côté routeur, il faudra récupérer un IP en DHCP (voir le mémo Cisco)
    ```
    R1#conf t
    Enter configuration commands, one per line.  End with CNTL/Z.
    R1(config)#interface fastEthernet 1/0
    R1(config-if)#ip address dhcp
    R1(config-if)#no shut
    R1(config-if)#exit
    R1(config-if)#exit
    R1(config)#exit
    R1#show ip int br
    Interface                  IP-Address      OK? Method Status                Protocol
    [...]
    FastEthernet1/0            10.0.3.16       YES DHCP   up                    up
    ```
- vous devriez pouvoir ping 1.1.1.1
    ```
    R1#ping 1.1.1.1

    Type escape sequence to abort.
    Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
    .!!!!
    Success rate is 80 percent (4/5), round-trip min/avg/max = 32/45/72 ms
    ```

**🌞 Configurez le NAT**

- référez-vous à la section NAT du mémo Cisco

    ```
    R1#conf t
    Enter configuration commands, one per line.  End with CNTL/Z.
    R1(config)#interface fastEthernet 1/0
    R1(config-if)#ip nat outside
    *Mar  1 02:10:54.867: %LINEPROTO-5-UPDOWN: Line protocol on Interface NVI0, changed state to up
    R1(config-if)#exit
    R1(config)#
    R1(config)#interface fastEthernet 0/0
    R1(config-if)#ip nat inside
    R1(config-if)#exit
    R1(config)#access-list 1 permit any
    R1(config)#ip nat inside source list 1 interface fastEthernet 1/0 overload
    ```

**🌞 Test**

- ajoutez une route par défaut (si c'est pas déjà fait)
    - sur les VPCS
    - sur la machine Linux
- configurez l'utilisation d'un DNS
    - sur les VPCS
        ```
        PC1> ip dns 1.1.1.1

        PC1> show ip all

        NAME   IP/MASK              GATEWAY           MAC                DNS
        PC1    10.1.1.1/24          10.1.1.254        00:50:79:66:68:00  1.1.1.1
        ```
        ```
        PC2> ip dns 1.1.1.1

        PC2> show ip all

        NAME   IP/MASK              GATEWAY           MAC                DNS
        PC2    10.1.1.2/24          10.1.1.254        00:50:79:66:68:01  1.1.1.1
        ```
        ```
        PC3> ip dns 1.1.1.1

        PC3> show ip all

        NAME   IP/MASK              GATEWAY           MAC                DNS
        PC3    10.2.2.1/24          10.2.2.254        00:50:79:66:68:02  1.1.1.1
        ```
    - sur la machine Linux
        ```
        [clem@web1 ~]$ sudo cat /etc/resolv.conf
        # Generated by NetworkManager
        search servers.tp4
        nameserver 1.1.1.1
        ```
- vérifiez un ping vers un nom de domaine
    ```
    PC1> ping google.com
    google.com resolved to 142.250.179.110

    84 bytes from 142.250.179.110 icmp_seq=1 ttl=113 time=41.336 ms
    84 bytes from 142.250.179.110 icmp_seq=2 ttl=113 time=29.051 ms
    ```
    ```
    PC2> ping gitlab.com
    gitlab.com resolved to 172.65.251.78

    84 bytes from 172.65.251.78 icmp_seq=1 ttl=53 time=52.670 ms
    84 bytes from 172.65.251.78 icmp_seq=2 ttl=53 time=44.258 ms
    ```
    ```
    PC3> ping ynov.com
    ynov.com resolved to 92.243.16.143

    84 bytes from 92.243.16.143 icmp_seq=1 ttl=49 time=47.000 ms
    84 bytes from 92.243.16.143 icmp_seq=2 ttl=49 time=47.912 ms
    ```
    ```
    [clem@web1 ~]$ ping -c 2 google.com
    PING google.com (172.217.19.238) 56(84) bytes of data.
    64 bytes from par21s11-in-f14.1e100.net (172.217.19.238): icmp_seq=1 ttl=112 time=38.2 ms
    64 bytes from par21s11-in-f14.1e100.net (172.217.19.238): icmp_seq=2 ttl=112 time=39.2 ms

    --- google.com ping statistics ---
    2 packets transmitted, 2 received, 0% packet loss, time 1001ms
    rtt min/avg/max/mdev = 38.229/38.708/39.188/0.518 ms
    ```
    
## V. Add a building

### 1. Topologie 5

### 2. Adressage topologie 5

### 3. Setup topologie 5

**🌞  Vous devez me rendre le show running-config de tous les équipements**

- de tous les équipements réseau
    - le routeur
        ```
        R1#show running-config
        Building configuration...

        Current configuration : 1546 bytes
        !
        version 12.4
        service timestamps debug datetime msec
        service timestamps log datetime msec
        no service password-encryption
        !
        hostname R1
        !
        boot-start-marker
        boot-end-marker
        !
        !
        no aaa new-model
        memory-size iomem 5
        no ip icmp rate-limit unreachable
        !
        !
        ip cef
        no ip domain lookup
        !
        !
        ip tcp synwait-time 5
        !
        !
        interface FastEthernet0/0
         no ip address
         ip nat inside
         ip virtual-reassembly
         duplex auto
         speed auto
        !
        interface FastEthernet0/0.10
         encapsulation dot1Q 10
         ip address 192.168.1.254 255.255.255.0
        !
        interface FastEthernet0/0.11
         encapsulation dot1Q 11
         ip address 10.1.1.254 255.255.255.0
        !
        interface FastEthernet0/0.12
         encapsulation dot1Q 12
         ip address 10.2.2.254 255.255.255.0
        !
        interface FastEthernet0/0.13
         encapsulation dot1Q 13
         ip address 10.3.3.254 255.255.255.0
        !
        interface FastEthernet0/0.20
         encapsulation dot1Q 20
         ip address 192.168.2.254 255.255.255.0
        !
        interface FastEthernet1/0
         ip address dhcp
         ip nat outside
         ip virtual-reassembly
         duplex auto
         speed auto
        !
        interface FastEthernet2/0
         no ip address
         shutdown
         duplex auto
         speed auto
        !
        interface FastEthernet3/0
         no ip address
         shutdown
         duplex auto
         speed auto
        !
        !
        no ip http server
        ip forward-protocol nd
        !
        !
        ip nat inside source list 1 interface FastEthernet1/0 overload
        !
        access-list 1 permit any
        no cdp log mismatch duplex
        !
        !
        control-plane
        !
        !
        line con 0
         exec-timeout 0 0
         privilege level 15
         logging synchronous
        line aux 0
         exec-timeout 0 0
         privilege level 15
         logging synchronous
        line vty 0 4
         login
        !
        !
        end
        ```
    - les 3 switches
        - sw1 : 
            ```
            Switch#show running-config
            Building configuration...

            Current configuration : 3773 bytes
            !
            ! Last configuration change at 14:45:09 UTC Sat Nov 6 2021
            !
            version 15.2
            service timestamps debug datetime msec
            service timestamps log datetime msec
            no service password-encryption
            service compress-config
            !
            hostname Switch
            !
            boot-start-marker
            boot-end-marker
            !
            !
            no aaa new-model
            !
            !
            ip cef
            no ipv6 cef
            !
            !
            spanning-tree mode pvst
            spanning-tree extend system-id
            !
            vlan internal allocation policy ascending
            !
            !
            interface GigabitEthernet0/0
             switchport access vlan 11
             switchport mode access
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet0/1
             switchport trunk encapsulation dot1q
             switchport mode trunk
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet0/2
             switchport access vlan 11
             switchport mode access
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet0/3
             switchport access vlan 12
             switchport mode access
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet1/0
             switchport access vlan 13
             switchport mode access
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet1/1
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet1/2
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet1/3
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet2/0
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet2/1
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet2/2
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet2/3
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet3/0
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet3/1
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet3/2
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet3/3
             media-type rj45
             negotiation auto
            !
            ip forward-protocol nd
            !
            no ip http server
            no ip http secure-server
            !
            !
            control-plane
            !
            banner exec ^C
            **************************************************************************
            * IOSv is strictly limited to use for evaluation, demonstration and IOS  *
            * education. IOSv is provided as-is and is not supported by Cisco's      *
            * Technical Advisory Center. Any use or disclosure, in whole or in part, *
            * of the IOSv Software or Documentation to any third party for any       *
            * purposes is expressly prohibited except as otherwise authorized by     *
            * Cisco in writing.                                                      *
            **************************************************************************^C
            banner incoming ^C
            **************************************************************************
            * IOSv is strictly limited to use for evaluation, demonstration and IOS  *
            * education. IOSv is provided as-is and is not supported by Cisco's      *
            * Technical Advisory Center. Any use or disclosure, in whole or in part, *
            * of the IOSv Software or Documentation to any third party for any       *
            * purposes is expressly prohibited except as otherwise authorized by     *
            * Cisco in writing.                                                      *
            **************************************************************************^C
            banner login ^C
            **************************************************************************
            * IOSv is strictly limited to use for evaluation, demonstration and IOS  *
            * education. IOSv is provided as-is and is not supported by Cisco's      *
            * Technical Advisory Center. Any use or disclosure, in whole or in part, *
            * of the IOSv Software or Documentation to any third party for any       *
            * purposes is expressly prohibited except as otherwise authorized by     *
            * Cisco in writing.                                                      *
            **************************************************************************^C
            !
            line con 0
            line aux 0
            line vty 0 4
             login
            !
            !
            end
            ```
        - sw2 : 
            ```
            Switch#show running-config
            Building configuration...

            Current configuration : 3684 bytes
            !
            ! Last configuration change at 17:17:27 UTC Sat Nov 6 2021
            !
            version 15.2
            service timestamps debug datetime msec
            service timestamps log datetime msec
            no service password-encryption
            service compress-config
            !
            hostname Switch
            !
            boot-start-marker
            boot-end-marker
            !
            !
            !
            no aaa new-model
            !
            !
            !
            !
            !
            !
            !
            !
            ip cef
            no ipv6 cef
            !
            !
            !
            spanning-tree mode pvst
            spanning-tree extend system-id
            !
            vlan internal allocation policy ascending
            !
            !
            !
            !
            !
            !
            !
            !
            !
            !
            !
            !
            !
            !
            interface GigabitEthernet0/0
             switchport trunk encapsulation dot1q
             switchport mode trunk
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet0/1
             switchport trunk encapsulation dot1q
             switchport mode trunk
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet0/2
             switchport trunk encapsulation dot1q
             switchport mode trunk
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet0/3
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet1/0
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet1/1
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet1/2
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet1/3
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet2/0
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet2/1
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet2/2
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet2/3
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet3/0
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet3/1
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet3/2
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet3/3
             media-type rj45
             negotiation auto
            !
            ip forward-protocol nd
            !
            no ip http server
            no ip http secure-server
            !
            !
            !
            !
            !
            !
            control-plane
            !
            banner exec ^C
            **************************************************************************
            * IOSv is strictly limited to use for evaluation, demonstration and IOS  *
            * education. IOSv is provided as-is and is not supported by Cisco's      *
            * Technical Advisory Center. Any use or disclosure, in whole or in part, *
            * of the IOSv Software or Documentation to any third party for any       *
            * purposes is expressly prohibited except as otherwise authorized by     *
            * Cisco in writing.                                                      *
            **************************************************************************^C
            banner incoming ^C
            **************************************************************************
            * IOSv is strictly limited to use for evaluation, demonstration and IOS  *
            * education. IOSv is provided as-is and is not supported by Cisco's      *
            * Technical Advisory Center. Any use or disclosure, in whole or in part, *
            * of the IOSv Software or Documentation to any third party for any       *
            * purposes is expressly prohibited except as otherwise authorized by     *
            * Cisco in writing.                                                      *
            **************************************************************************^C
            banner login ^C
            **************************************************************************
            * IOSv is strictly limited to use for evaluation, demonstration and IOS  *
            * education. IOSv is provided as-is and is not supported by Cisco's      *
            * Technical Advisory Center. Any use or disclosure, in whole or in part, *
            * of the IOSv Software or Documentation to any third party for any       *
            * purposes is expressly prohibited except as otherwise authorized by     *
            * Cisco in writing.                                                      *
            **************************************************************************^C
            !
            line con 0
            line aux 0
            line vty 0 4
            !
            !
            end
            ```

        - sw3 : 
            ```
            Switch#show running-config
            Building configuration...

            Current configuration : 3803 bytes
            !
            ! Last configuration change at 17:38:39 UTC Sat Nov 6 2021
            !
            version 15.2
            service timestamps debug datetime msec
            service timestamps log datetime msec
            no service password-encryption
            service compress-config
            !
            hostname Switch
            !
            boot-start-marker
            boot-end-marker
            !
            !
            no aaa new-model
            !
            !
            ip cef
            no ipv6 cef
            !
            !
            spanning-tree mode pvst
            spanning-tree extend system-id
            !
            vlan internal allocation policy ascending
            !
            !
            interface GigabitEthernet0/0
             switchport trunk allowed vlan 11-13
             switchport trunk encapsulation dot1q
             switchport mode trunk
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet0/1
             switchport access vlan 11
             switchport mode access
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet0/2
             switchport access vlan 11
             switchport mode access
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet0/3
             switchport access vlan 11
             switchport mode access
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet1/0
             switchport access vlan 11
             switchport mode access
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet1/1
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet1/2
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet1/3
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet2/0
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet2/1
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet2/2
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet2/3
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet3/0
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet3/1
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet3/2
             media-type rj45
             negotiation auto
            !
            interface GigabitEthernet3/3
             media-type rj45
             negotiation auto
            !
            ip forward-protocol nd
            !
            no ip http server
            no ip http secure-server
            !
            !
            control-plane
            !
            banner exec ^C
            **************************************************************************
            * IOSv is strictly limited to use for evaluation, demonstration and IOS  *
            * education. IOSv is provided as-is and is not supported by Cisco's      *
            * Technical Advisory Center. Any use or disclosure, in whole or in part, *
            * of the IOSv Software or Documentation to any third party for any       *
            * purposes is expressly prohibited except as otherwise authorized by     *
            * Cisco in writing.                                                      *
            **************************************************************************^C
            banner incoming ^C
            **************************************************************************
            * IOSv is strictly limited to use for evaluation, demonstration and IOS  *
            * education. IOSv is provided as-is and is not supported by Cisco's      *
            * Technical Advisory Center. Any use or disclosure, in whole or in part, *
            * of the IOSv Software or Documentation to any third party for any       *
            * purposes is expressly prohibited except as otherwise authorized by     *
            * Cisco in writing.                                                      *
            **************************************************************************^C
            banner login ^C
            **************************************************************************
            * IOSv is strictly limited to use for evaluation, demonstration and IOS  *
            * education. IOSv is provided as-is and is not supported by Cisco's      *
            * Technical Advisory Center. Any use or disclosure, in whole or in part, *
            * of the IOSv Software or Documentation to any third party for any       *
            * purposes is expressly prohibited except as otherwise authorized by     *
            * Cisco in writing.                                                      *
            **************************************************************************^C
            !
            line con 0
            line aux 0
            line vty 0 4
            !
            !
            end
            ```

